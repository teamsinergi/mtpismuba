<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() 
	{ 
		parent::__construct();
		$this->load->model('m_buku');
		$this->load->helper('url'); 
	}
	public function index()
	{
        $data['bukuku']=$this->m_buku->getAllsatu();
        $data['buku'] = $this->m_buku->buku_terlaris();
        $data['buku4'] = $this->m_buku->buku_cor();
        $data['buku5'] = $this->m_buku->buku_corb();
        $data['buku6'] = $this->m_buku->buku_terlaris_sd();
        $data['buku7'] = $this->m_buku->buku_terlaris_smp();
        $data['buku8'] = $this->m_buku->buku_terlaris_sma();
        $this->load->view("customer/main", $data);
	}
}
