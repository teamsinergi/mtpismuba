<?php
class Buku1 extends CI_Controller{
	function __construct(){
		parent::__construct();
        $this->load->model('m_buku');
        $this->load->model('m_stok');
        $this->load->library('form_validation');
        $this->load->library('upload');
	}

    
	function index(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
            $data["buku"] = $this->m_buku->getAll();
            $this->load->view("admin/buku/v_tampil_buku", $data);
        }

	}
    
    public function add(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $this->load->view("admin/buku/v_tambah_buku");
        }
    }

    public function edit($id){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $where = array('id_buku'=>$id);
        $data['buku']=$this->m_buku->edit_buku($where, 'tbl_buku');
        $this->load->view("admin/buku/v_edit_buku", $data);
        }
    }

    public function update_buku(){

        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $gambar=$this->input->post('gambar');
                $path='./assets/images/'.$gambar;
                unlink($path);

                $photo=$gbr['file_name'];
                $id=$this->input->post('xid');
                $nama=$this->input->post('xnama');
                $kelas = $this->input->post('xkelas');
                $harga = $this->input->post('xharga');
                $stok = $this->input->post('xstok');
                $query=$this->m_buku->update_buku($id, $nama, $kelas, $harga, $stok, $photo);
                $this->m_buku->update_laporan_stok($id,$stok);
                if($query){
                    $this->session->set_flashdata('pesan','Buku berhasil di update');
                }
                redirect('admin/buku1/');
            }else{
                redirect('admin/buku1/');
            }

        }else{

            $id=$this->input->post('xid');
            $nama=$this->input->post('xnama');
            $kelas = $this->input->post('xkelas');
            $harga = $this->input->post('xharga');
            $stok = $this->input->post('xstok');
            $query = $this->m_buku->update_buku_tanpa_img($id, $nama, $kelas, $harga, $stok);
            $this->m_buku->update_laporan_stok($id,$stok);
            if($query){
                $this->session->set_flashdata('pesan','Buku berhasil di update');
            }
            redirect('admin/buku1/');
        }



        $id=$this->input->post('xid');
        $nama=$this->input->post('xnama');
        $kelas = $this->input->post('xkelas');
        $harga = $this->input->post('xharga');
        $stok = $this->input->post('xstok');
        $this->m_buku->update_buku($id, $nama, $kelas, $harga, $stok);
        redirect('admin/buku1/');
        
    }

    public function hapus($id){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $where = array('id_buku'=>$id);
        $this->m_buku->hapus($where, 'buku');
        redirect('admin/buku1/');
        }
    }

    public function simpan_buku(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $config['upload_path'] = './assets/images/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();

                $config['image_library']='gd2';
                $config['source_image']='./assets/images/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                
                $photo=$gbr['file_name'];
                $nama = $this->input->post("xnama");
                $kelas = $this->input->post("xkelas");
                $harga = $this->input->post("xharga");
                $stok = $this->input->post("xstok");
                $query = $this->m_buku->simpan_buku($nama, $kelas, $harga, $stok,$photo);
                $idbukunya = $this->m_buku->ambil_id();
                $this->m_buku->simpan_stok($idbukunya, $stok);
                $this->m_buku->simpan_tahun_sebelumnya($idbukunya, $stok);
                $this->m_stok->simpan_sisa($idbukunya, $stok);
                $this->m_stok->simpan_tahun_sebelumnya_sisa($idbukunya, $stok);
                if($query){
                    $this->session->set_flashdata('pesan','Buku berhasil di simpan');
                }
                redirect('admin/buku1/');
            }else{
                redirect('admin/buku1/');
            }

        }else{
            $nama = $this->input->post("xnama");
            $kelas = $this->input->post("xkelas");
            $harga = $this->input->post("xharga");
            $stok = $this->input->post("xstok");
            $this->m_buku->simpan_buku_tanpa_image($nama, $kelas, $harga, $stok);
            $idbukunya = $this->m_buku->ambil_id();
            $this->m_buku->simpan_stok($idbukunya, $stok);
            $this->m_buku->simpan_tahun_sebelumnya($idbukunya, $stok);
            $this->m_stok->simpan_sisa($idbukunya, $stok);
            $this->m_stok->simpan_tahun_sebelumnya_sisa($idbukunya, $stok);
            redirect('admin/buku1/');
        }
        }
    }

    public function delete_buku(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $id = $this->input->post('xid');
		$gambar=$this->input->post('foto_barang');
		$path='./assets/images/'.$gambar;
        unlink($path);

        $query = $this->m_buku->delete_buku($id);
        if($query){
            $this->session->set_flashdata('pesan','Buku berhasil di hapus');
        }
        redirect('admin/buku1/');
        }
    }

    

}