<?php

class User extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->model('M_user');
                $this->load->library('session');

    }

    public function index(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
            redirect('admin/overview');
        }
        
    }

    public function register_user(){

            $user=array(
            'user_name'=>$this->input->post('user_name'),
            'user_email'=>$this->input->post('user_email'),
            'user_password'=>md5($this->input->post('user_password')),
                );
                print_r($user);

        $email_check=$this->M_user->email_check($user['user_email']);

        if($email_check){
        $this->M_user->register_user($user);
        $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
        redirect('admin/user/login_view');

        }
        else{
        $this->session->set_flashdata('error_msg', 'Error occured,Try again.');
        redirect('admin/user');
        }

    }

    public function login_view(){
        $this->load->view("admin/user/v_login");

    }

    function login_user(){
        $user_login=array(
        'user_email'=>$this->input->post('user_email'),
        'user_password'=>md5($this->input->post('user_password'))
            );

            $data=$this->M_user->login_user($user_login['user_email'],$user_login['user_password']);
            if($data)
            {
                $this->session->set_userdata('id_user',$data['id_user']);
                $this->session->set_userdata('user_email',$data['user_email']);
                $this->session->set_userdata('user_name',$data['user_name']);
                redirect('admin/overview');

            }
            else{
                $this->session->set_flashdata('error_msg', 'Gagal.! Email Atau Password Salah');
                $this->load->view("admin/user/v_login.php");

            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function user_logout(){
        $this->session->sess_destroy();
        redirect('admin/user/login_view', 'refresh');
    }

    function tampil_user(){
        $data['pengguna']=$this->M_user->admin_user();
		$this->load->view('admin/user/v_admin_user',$data);
    }


        public function edit_user($id){
        $where = array('id_user'=>$id);
        $data['user']=$this->M_user->edit_user($where, 'tbl_user'); 
        $this->load->view("admin/user/v_edit_user", $data);
    }

    public function update_user(){
        $id=$this->input->post('xid');
        $name=$this->input->post('xname');
        $email = $this->input->post('xemail');
        $pass = md5($this->input->post('xpass'));
        $this->M_user->update_user($id, $name, $email, $pass);
        redirect('admin/user/');
        
    }


      public function simpan_user(){
        $id_user = $this->input->post('xid');
        $user_name = $this->input->post('xname');
        $user_email = $this->input->post('xemail');
        $user_password = md5($this->input->post('xpass'));
        $this->M_user->simpan_user($id_user, $user_name, $user_email, $user_password);
        redirect('admin/user/Tampil_user');
    }

        public function delete_user(){
            $iduser= $this->input->post("xid");
            $this->M_user->delete_user($iduser);
            redirect('admin/user/Tampil_user','refresh');
    }

    public function daftar_admin(){
        $this->load->view('admin/user/v_register');
    }

}

?>