<?php
class Transaksi extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_transaksi');
		$this->load->model('m_buku');
		$this->load->model('m_pesanan');
		$this->load->library('upload');
	}


	function index(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
			$data['tabletransaksi']=$this->m_transaksi->get_transaksi();
			$data['transaksi']=$this->m_transaksi->get_all_transaksi();
			$data['totalharganya'] = $this->m_transaksi->total_harganya();
			$this->load->view('admin/transaksi/v_tampil_transaksi', $data);
		}
	}
	
	function add(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$this->m_transaksi->hapustampung();
		$data['sekolah'] = $this->m_transaksi->get_sekolah();
		$data['buku'] = $this->m_transaksi->get_buku();
		$this->load->view('admin/transaksi/v_tambah_transaksi', $data);
		}
	}

	function simpan_transaksi(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$idsekolah = $this->input->post('xsekolah');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$jumlah = count($idbarang);
		$y =0;
		for($x=0;$x<$jumlah;$x++){
			while($banyakbarang[$y]==null){
			$y++;
			}
			$harga = $this->m_transaksi->ambil_harga($idbarang[$x]);
			$this->m_transaksi->simpan_transaksi($idsekolah, $idbarang[$x], $banyakbarang[$y], $harga);
			$this->m_transaksi->update_stok($idbarang[$x], $banyakbarang[$y]);
			$this->m_transaksi->update_laporan_stok($idbarang[$x], $banyakbarang[$y]);
			$y++;
		}
		redirect('admin/transaksi/kecil');
		}
	}

	function delete_sekolah(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$id = $this->input->post('xid');
		$this->m_transaksi->delete_sekolah($id);
		redirect('admin/transaksi');
		}
	}

	function kecil(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		// $data['tabletransaksi']=$this->m_transaksi->get_transaksi();
		$data['transaksi']=$this->m_transaksi->get_all_transaksi();
		$data['kecil']=$this->m_transaksi->transaksi_kecil();
		$this->load->view('admin/transaksi/v_transaksi_kecil', $data);
		}
	}

	public function lanjutkan($where, $tanggal, $kode){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['detailtransaksi']=$this->m_transaksi->detail_transaksi($where, $kode);
		$data['transaksi']=$this->m_transaksi->transaksi($kode);
		$data['totalharga']=$this->m_transaksi->detail_harga($where, $kode);
		$data['pelanggan']=$this->m_transaksi->nama_sekolah($where);
		$data['tanggalnota'] = $tanggal;
		$data['idsekolahnota'] = $where;
		$data['kode'] = $kode;
		$data['status'] = $this->m_transaksi->status_barang();
		$this->load->view("admin/transaksi/v_transaksi_lanjut", $data);
		}
	}

	public function cetak_nota($id_sekolah, $tanggal, $kode){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['detailtransaksi']=$this->m_transaksi->detail_transaksi($id_sekolah, $kode);
		$data['totalharga']=$this->m_transaksi->detail_harga_rev($id_sekolah, $kode);
		//$data['totalharga']=$this->m_transaksi->detail_harga($id_sekolah, $kode);
		
		$data['pelanggan']=$this->m_transaksi->nama_sekolah($id_sekolah);
		$data['tanggalnota'] = $tanggal;
		$data['idsekolahnota'] = $id_sekolah;
		$data['kodetransaksi'] = $kode;
		$data['tanggalprint'] = date('d/M/Y');
		$data['alamatpengiriman'] = $this->m_transaksi->alamat_pengiriman($kode);
		$this->load->view("admin/transaksi/v_cetak_nota", $data);
		}
	}
	
	function masuk(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['pesanan']=$this->m_pesanan->pesananmasuk();
		$this->load->view('admin/transaksi/v_transaksi_masuk', $data);
		}
	}

	function masuk_detail($idpembeli, $id_transaksi){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['detailpesanan']=$this->m_pesanan->detail_pesanan($idpembeli, $id_transaksi);
		$data['detailpembeli']=$this->m_pesanan->detail_pembeli($idpembeli, $id_transaksi);
		$data['totalharga']=$this->m_pesanan->total_harga($idpembeli, $id_transaksi);
		$data['alamat'] = $this->m_pesanan->alamat_tujuan($id_transaksi);
		$data['kode_transaksi'] = $id_transaksi;
		$this->load->view('admin/transaksi/v_transaksi_masuk_detail', $data);
		}
	}

	function simpan_transaksi_pesanan(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$idsekolah = $this->input->post('xcustomer');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$totalharga = $this->input->post('xtotal');
		$idtransaksi = $this->input->post('xidtransaksi');
		$jumlah = count($idbarang);
		$tujuan = $this->input->post("xtujuan");
		for($x=0;$x<$jumlah;$x++){
			$query = $this->m_transaksi->simpan_transaksi_pesanan($idsekolah, $idbarang[$x], $banyakbarang[$x], $totalharga[$x], $idtransaksi, $tujuan);
			$this->m_transaksi->update_stok($idbarang[$x], $banyakbarang[$x]);
			$this->m_transaksi->update_laporan_stok($idbarang[$x], $banyakbarang[$x]);
		}
		if($query){
			$this->session->set_flashdata('pesan','Transaksi Berhasil');
		}
		$this->m_pesanan->hapus_pesanan($idsekolah, $idtransaksi);
		redirect('admin/transaksi/kecil');
		}
	}

	function simpan_transaksi_tampung(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$idsekolah = $this->input->post('xcustomer');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$totalharga = $this->input->post('xtotal');
		$idtransaksi = $this->input->post('xidtransaksi');
		$tujuan = $this->input->post("xtujuan");
		$jumlah = count($idbarang);
		for($x=0;$x<$jumlah;$x++){
			$query = $this->m_transaksi->simpan_transaksi_pesanan($idsekolah, $idbarang[$x], $banyakbarang[$x], $totalharga[$x], $idtransaksi, $tujuan);
			$this->m_transaksi->update_stok($idbarang[$x], $banyakbarang[$x]);
			$this->m_transaksi->update_laporan_stok($idbarang[$x], $banyakbarang[$x]);
		}
		$this->m_transaksi->hapustampung();
		if($query){
			$this->session->set_flashdata('pesan','Transaksi berhasil');
		}
		redirect('admin/transaksi/kecil');
		}
	}
	
	public function edit($idtransaksi){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['edit_transaksi']=$this->m_transaksi->edit_transaksi($idtransaksi);
		$this->load->view("admin/transaksi/v_edit_transaksi", $data);
		}
	}

	public function update_transaksi(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$id = $this->input->post("xid");
		$banyak = $this->input->post("xbanyak");
		$this->m_transaksi->updatetransaksi($id, $banyak);
		redirect('admin/transaksi/kecil');
		}
	}

	public function hapus_tranasksi($id_transaksi, $id_sekolah){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$this->m_transaksi->hapustranasksi($id_transaksi, $id_sekolah);
		redirect('admin/transaksi/kecil');
		}
	}

	public function tampung_transaksi(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$idsekolah = $this->input->post('xsekolah');
		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
		$jumlah = count($idbarang);
		$y =0;
		for($x=0;$x<$jumlah;$x++){
			while($banyakbarang[$y]==null){
			$y++;
			}
			$harga = $this->m_transaksi->ambil_harga($idbarang[$x]);
			$this->m_transaksi->tampung_transaksi($idsekolah, $idbarang[$x], $banyakbarang[$y], $harga);
			$y++;
		}
		
		redirect('admin/transaksi/konfirmasi');
		}
	}

	public function konfirmasi(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['detailpesanan'] = $this->m_transaksi->lihat_tampung();
		$data['kode'] = $this->m_transaksi->kode_transaksi();
		$data['alamat'] = $this->m_transaksi->alamat_tujuan();
		$data['totalharga'] = $this->m_transaksi->total_harga();
		$this->load->view("admin/transaksi/v_tampung_transaksi", $data);
		}
	}

	public function batal(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$query = $this->m_transaksi->hapustampung();
		if($query){
			$this->session->set_flashdata('pesan','Transaksi di batalkan');
		}
		redirect('admin/transaksi/kecil');
		}
	}

	public function status_barang(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$status = $this->input->post('xstatus');
		$kode = $this->input->post('xkode');
		$this->m_transaksi->update_status($status, $kode);
		redirect('admin/transaksi/kecil');
		}
	}

	public function bukti_transfer(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$data['bukti'] = $this->m_transaksi->bukti_transfer();
		$this->load->view('admin/bukti/v_bukti_transfer', $data);
		}
	}
	public function batalkan($kode){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
		$this->m_transaksi->batalkan($kode);
		redirect('admin/transaksi/masuk');
		}
	}   
}