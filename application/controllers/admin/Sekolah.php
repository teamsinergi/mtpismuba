<?php
class Sekolah extends CI_Controller{
	function __construct(){
		parent::__construct();
        $this->load->model('m_sekolah');
        $this->load->model('m_kabupaten');
		$this->load->library('upload');
	}
	function index(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $data['sekolah']=$this->m_sekolah->get_all_sekolah1();
        $this->load->view('admin/sekolah/v_tampil_sekolah',$data);
        }
    }
    
    public function add(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $data["kabupaten"] = $this->m_kabupaten->getAll();
        $data['level'] = $this->m_sekolah->level_customer();
        $data['provinsi'] = $this->m_kabupaten->get_all_provinsi();
        $this->load->view('admin/sekolah/v_tambah_sekolah', $data);
        }
    }

    public function simpan_sekolah(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $nama_sekolah = $this->input->post('xnamasekolah');
        $alamat_sekolah = $this->input->post('xalamatsekolah');
        $idkabupaten = $this->input->post('xidkabupaten');
        $email = $this->input->post('xemail');
        $nohp = $this->input->post('xnomer');
        $level = $this->input->post('xidlevel');
        $pass = md5($this->input->post('xpassword'));
        $query = $this->m_sekolah->simpan_sekolah($nama_sekolah, $alamat_sekolah, $idkabupaten, $email, $nohp, $pass, $level);
        if($query){
            $this->session->set_flashdata('pesan','Pelanggan berhasil di tambah');
        }
        redirect('admin/sekolah');
        }
    }
    
    public function delete_sekolah(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $idsekolah = $this->input->post("xid");
        $query = $this->m_sekolah->delete_sekolah($idsekolah);
        $this->m_sekolah->delete_transaksi_sekolah($idsekolah);
        if($query){
            $this->session->set_flashdata('pesan','Pelanggan berhasil di hapus');
        }
        redirect('admin/sekolah/','refresh');
        }
    }

    public function edit($id){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $where = array('id_sekolah'=>$id);
        $data["kabupaten"] = $this->m_kabupaten->getAll();
        $data['sekolah']=$this->m_sekolah->edit_sekolah($where, 'tbl_sekolah')->result();
        $data['level'] = $this->m_sekolah->level_customer();
        $data['nama_level'] = $this->m_sekolah->nama_levelnya($id);
        $data['id_level'] = $this->m_sekolah->id_levelnya($id);
        $data['nama_kabupaten'] = $this->m_kabupaten->nama_kabupatennya($id);
        $data['id_kabupaten'] = $this->m_kabupaten->id_kabupatennya($id);
        $data['provinsi'] = $this->m_kabupaten->get_all_provinsi();
        $this->load->view("admin/sekolah/v_edit_sekolah", $data);
        }
    }

    public function update_sekolah(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $id = $this->input->post('xid');
        $nama = $this->input->post('xnama');
        $alamat = $this->input->post('xalamat');
        $idkab = $this->input->post('xidkab');
        $level = $this->input->post('xidlevel');
        $email = $this->input->post('xemail');
        $nohp = $this->input->post('xnomer');
        $pass = md5($this->input->post('xpassword'));
        $query = $this->m_sekolah->update_sekolah($id, $nama, $alamat, $idkab, $email, $nohp, $pass, $level);
        if($query){
            $this->session->set_flashdata('pesan','Pelanggan berhasil di update');
        }
        redirect('admin/sekolah');
        }
    }
}