<?php 


class Contact_us extends CI_Controller{
    public function __construct(){
        parent::__construct();
            $this->load->helper('url');
            $this->load->model('m_komentar');
            $this->load->library('Session');
    }
    public function index()
    {
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
            $data['komentar']= $this->m_komentar->tampil_komentar();
            $this->load->view("admin/contact_us/v_contact_us", $data);
        }
        
    }
    function hapus($id){
        $this->m_komentar->hapus_komentar($id);
        redirect('admin/contact_us');
    }

}

?>