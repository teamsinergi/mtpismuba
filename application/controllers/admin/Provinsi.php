<?php
class Provinsi extends CI_Controller{
	function __construct(){
		parent::__construct();
        $this->load->model('m_provinsi');
		$this->load->library('form_validation');
	}


	function index(){
		if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $data["provinsi"] = $this->m_provinsi->getAll();
        $this->load->view("admin/provinsi/v_tampil_provinsi", $data);
		}
	}

}