<?php
class Laporan extends CI_Controller{
	function __construct(){
        parent::__construct();
        $this->load->model('m_transaksi');
        $this->load->model('m_laporan');
        $this->load->model('m_sekolah');
        $this->load->model('m_overview');
        $this->load->model('m_stok');
        $this->load->library('upload');
	}


	function index(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
            $data['tampilsemua'] = $this->m_laporan->tampil_semua();
            $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual();
            $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan();
            $data['total']=$this->m_transaksi->banyak_transaksi();
            $data['kabupaten']=$this->m_laporan->getkabupaten();
            $data['provinsi'] = $this->m_laporan->getprovinsi();
            $data['kabprov'] = $this->m_laporan->kab_prov();
            $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten();
            $data['pilihkabupaten'] = $this->m_laporan->pilih_kabupaten();
            $data['laporanbuku'] = $this->m_laporan->laporan_buku();
            $this->load->view('admin/laporan/v_tampil_laporan', $data);        
        }
    }

    function cetak_laporan(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $data['tampilsemua'] = $this->m_laporan->tampil_semua();
        $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual();
        $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan();
        $data['total']=$this->m_transaksi->banyak_transaksi();
        $data['kabupaten']=$this->m_laporan->getkabupaten();
        $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten();
        $data['laporanbuku'] = $this->m_laporan->laporan_buku();
        $this->load->view('admin/laporan/v_cetak', $data);
        }
    }

    function ambil_id_kabupaten($idkab){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $this->m_laporan->ambil_total($idkab);
        }
    }


    //semua di bawah ini function buat cetak laporan perkabupaten

    function cetak_laporan_kabupaten(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $idkabupten = $this->input->post("xidkabupaten");
        $bulandari = $this->input->post("xbulan1");
        $bulansampai = $this->input->post("xbulan2");
        $tahundari = $this->input->post("xtahun1");
        // $tahunsampai = $this->input->post("xtahun2");
        $data['tampilsemua_kab'] = $this->m_laporan->tampil_semua_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['nama_sekolah_kab'] = $this->m_laporan->nama_sekolah_kabupaten($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['totalbukuterjual_kab'] = $this->m_laporan->total_buku_terjual_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['totalpendapatanpenjualan_kab'] = $this->m_laporan->total_pendapatan_kab($idkabupten, $bulandari, $tahundari, $bulansampai);
        $data['databuku'] = $this->m_laporan->banyak_buku();
        $this->load->view("admin/laporan/v_cetak_laporan_kab.php", $data);
        }
    }

    function cetak_laporan_buku(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $tahun = $this->input->post("xtahun1");
        $data['filtertahun'] = $tahun;
        $data['tahunsekarang'] = date('Y');
        $tahunlama = $tahun-1;
        $data['bukutahun'] = $this->m_laporan->stok_2019($tahun);
        $data['databuku'] = $this->m_laporan->laporan_bukuku($tahun);
        $data['rekapan'] = $this->m_laporan->rekapan_buku($tahun);
        $data['laporansisa'] = $this->m_laporan->laporan_sisa($tahun);
        $data['tahun'] = $tahun;
        $data['tahunlama'] = $tahunlama;
        $data['filterlevel'] = $this->m_laporan->filter_lelve();
        $data['model'] = $this->m_stok;
        // $data['tahunbuku'] = $this->m_laporan->tahun_buku();
        $this->load->view("admin/laporan/v_cetak_buku.php", $data);
        }
        
    }

    function filter() {
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $this->load->view("admin/laporan/v_filter.php");
        }
    }

    function cetak_laporan_semua(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        $tahundari = $this->input->post("xtahun1");
        $tahunsampai = $this->input->post("xtahun2");
        $data['tampilsemua'] = $this->m_laporan->tampil_semua_x($tahundari, $tahunsampai);
        $data['totalbukuterjual'] = $this->m_laporan->total_buku_terjual_x($tahundari, $tahunsampai);
        $data['totalpendapatanpenjualan'] = $this->m_laporan->total_pendapatan_x($tahundari, $tahunsampai);
        $data['total']=$this->m_transaksi->banyak_transaksi_x($tahundari, $tahunsampai);
        $data['kabupaten']=$this->m_laporan->getkabupaten_x($tahundari, $tahunsampai);
        $data['sekolahkabupaten']=$this->m_laporan->sekolah_kabupaten_x($tahundari, $tahunsampai);
        $data['laporanbuku'] = $this->m_laporan->laporan_buku_x($tahundari, $tahunsampai);
        $this->load->view('admin/laporan/v_cetak_laporan', $data);
        }
    }

    function export_excel(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
        // create file name
        $fileName = 'data-' . time() . '.xlsx';
        // load excel library
        $this->load->library('excel');

        // data awal
        $tahundari = $this->input->post("xtahun1");
        $tahunsampai = $this->input->post("xtahun2");
        $tampilsemua = $this->m_laporan->tampil_semua_excel($tahundari, $tahunsampai);
        $totalbukuterjual = $this->m_laporan->total_buku_terjual_excel($tahundari, $tahunsampai);
        $totalpendapatanpenjualan = $this->m_laporan->total_pendapatan_excel($tahundari, $tahunsampai);
        // $total=$this->m_transaksi->banyak_transaksi_excel($tahundari, $tahunsampai);
        // $kabupaten=$this->m_laporan->getkabupaten_x($tahundari, $tahunsampai);
        // $sekolahkabupaten=$this->m_laporan->sekolah_kabupaten_x($tahundari, $tahunsampai);
        // $laporanbuku = $this->m_laporan->laporan_buku_x($tahundari, $tahunsampai);

        // data awal akhir
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        // set Header untuk atas
        $objPHPExcel->getActiveSheet()->mergeCells('B3:D3');
        $objPHPExcel->getActiveSheet()->SetCellValue('B3', 'Laporan Penjualan Buku ISMUBA Tahun '.$tahundari);
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('B3:D3')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('B10')->setWidth(26);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('B4', 'Total Buku Terjual');
        $objPHPExcel->getActiveSheet()->SetCellValue('B5', 'Total Pendapatan');
        $objPHPExcel->getActiveSheet()->SetCellValue('B6', 'Total Transaksi');
        // set Row
        $rowCount = 4;
        foreach ($totalbukuterjual as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['totalbukuterjual']);
            $rowCount++;
        }
        foreach ($totalpendapatanpenjualan as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['totalharga']);
            $rowCount++;
        }

        // set Header untuk pembeli
        $objPHPExcel->getActiveSheet()->mergeCells('B9:D9');
        $objPHPExcel->getActiveSheet()->SetCellValue('B9', 'Daftar Pembeli');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('B9:D9')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->SetCellValue('A10', 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B10', 'Nama Pembeli');
        $objPHPExcel->getActiveSheet()->SetCellValue('C10', 'Kabupaten');
        $objPHPExcel->getActiveSheet()->SetCellValue('D10', 'Jumlah Pembelian');
        $objPHPExcel->getActiveSheet()->SetCellValue('E10', 'Total Harga');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('A10:B10')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('C10:D10')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('E10')->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        // set Row
        $rowCount = 11;
        $no = 0;
        foreach ($tampilsemua as $element) {
            $no++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['nama_sekolah']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['banyak']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['totalharga']);
            $rowCount++;
        }

        $rowCount = $rowCount +2;
        $rowCountkab = $rowCount+1;

        // set Header untuk daftar kabupaten
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$rowCount.':D'.$rowCount);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Daftar Kabupaten');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount.':D'.$rowCount)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCountkab, 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCountkab, 'Nama Kabupaten');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCountkab, 'Total Buku Terjual');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCountkab, 'Total Pendapatan');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowCountkab.':B'.$rowCountkab)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('C'.$rowCountkab.':D'.$rowCountkab)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        
        // set Row
        $rowCount = $rowCountkab+1;
        $no = 0;
        $perkabupaten = $this->m_laporan->laporan_perkabupaten($tahundari);
        foreach ($perkabupaten as $data) {
            $no++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data['banyakterjual']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data['totalharga']);
            $rowCount++;
        }

        $rowCount = $rowCount +2;
        $rowCountbuku = $rowCount+1;

        // set untuk daftar buku terjual
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$rowCount.':D'.$rowCount);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Daftar Buku Terjual');
        // untuk warna dan border
        $objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount.':D'.$rowCount)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCountbuku, 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCountbuku, 'Nama Buku');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCountbuku, 'Kelas');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCountbuku, 'Jumlah Terjual');
        $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCountbuku, 'Harga');
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCountbuku, 'Total Harga');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowCountbuku.':B'.$rowCountbuku)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('C'.$rowCountbuku.':D'.$rowCountbuku)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
                //buat kasi warna aja
                $objPHPExcel->getActiveSheet()->getStyle('E'.$rowCountbuku.':F'.$rowCountbuku)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '99ccff')
                        ),
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    )
                );
        
        // set Row
        $rowCount = $rowCountbuku+1;
        $no = 0;
        $perkabupaten = $this->m_laporan->buku_terjual($tahundari);
        foreach ($perkabupaten as $data) {
            $no++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data['nama_buku']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data['kelas_buku']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data['banyak_barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $data['harga_buku']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $data['total_harga']);
            $rowCount++;
        }
        $rowCount = $rowCount +2;
        $rowCountprov = $rowCount+1;
        // set Header untuk daftar kabupaten
        $objPHPExcel->getActiveSheet()->mergeCells('B'.$rowCount.':D'.$rowCount);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Daftar Provinsi');
        // untuk warna dan border
        $objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount.':D'.$rowCount)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'ffff00')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCountprov, 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCountprov, 'Nama Provinsi');
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCountprov, 'Total Buku Terjual');
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCountprov, 'Total Pendapatan');
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowCountprov.':B'.$rowCountprov)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        //buat kasi warna aja
        $objPHPExcel->getActiveSheet()->getStyle('C'.$rowCountprov.':D'.$rowCountprov)->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '99ccff')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            )
        );
        // set Row
        $rowCount = $rowCountprov+1;
        $no = 0;
        $perprov = $this->m_laporan->laporan_perpro($tahundari);
        foreach ($perprov as $data) {
            $no++;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $no);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $data['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $data['banyak_barang']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $data['total_harga']);
            $rowCount++;
        }
    
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(ROOT_UPLOAD_IMPORT_PATH . $fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(HTTP_UPLOAD_IMPORT_PATH . $fileName);
        }
    }

    public function backupdata(){
        if ($this->session->userdata('user_name')==null){
            $this->load->view("admin/user/v_login");
        }else{
            $data['hitungbuku'] = $this->m_overview->hitung_buku();
				$data['hitungpelanggan'] = $this->m_overview->hitung_pelanggan();
				$data['total']=$this->m_transaksi->banyak_transaksi();
				$data['terlaris'] = $this->m_overview->buku_terlaris();
				$data['sekolahterbanyak']= $this->m_overview->sekolah_terbanyak();
				$data['model'] = $this->m_laporan;
				// backup data
				// Database configuration
				$host = "localhost";
				$username = "root";
				$password = "";
				$database_name = "db_belibuku";
	
				// Get connection object and set the charset
				$conn = mysqli_connect($host, $username, $password, $database_name);
				$conn->set_charset("utf8");
	
	
				// Get All Table Names From the Database
				$tables = array();
				$sql = "SHOW TABLES";
				$result = mysqli_query($conn, $sql);
	
				while ($row = mysqli_fetch_row($result)) {
					$tables[] = $row[0];
				}
	
				$sqlScript = "";
				foreach ($tables as $table) {
					
					// Prepare SQLscript for creating table structure
					$query = "SHOW CREATE TABLE $table";
					$result = mysqli_query($conn, $query);
					$row = mysqli_fetch_row($result);
					
					$sqlScript .= "\n\n" . $row[1] . ";\n\n";
					
					
					$query = "SELECT * FROM $table";
					$result = mysqli_query($conn, $query);
					
					$columnCount = mysqli_num_fields($result);
					
					// Prepare SQLscript for dumping data for each table
					for ($i = 0; $i < $columnCount; $i ++) {
						while ($row = mysqli_fetch_row($result)) {
							$sqlScript .= "INSERT INTO $table VALUES(";
							for ($j = 0; $j < $columnCount; $j ++) {
								$row[$j] = $row[$j];
								
								if (isset($row[$j])) {
									$sqlScript .= '"' . $row[$j] . '"';
								} else {
									$sqlScript .= '""';
								}
								if ($j < ($columnCount - 1)) {
									$sqlScript .= ',';
								}
							}
							$sqlScript .= ");\n";
						}
					}
					
					$sqlScript .= "\n"; 
				}
	
				if(!empty($sqlScript))
				{
					$tanggal = date("Y-m-d");
					// Save the SQL script to a backup file
					$backup_file_name = $database_name . '_backup_' . $tanggal . '.sql';
					$fileHandler = fopen($backup_file_name, 'w+');
					$number_of_lines = fwrite($fileHandler, $sqlScript);
					fclose($fileHandler); 
	
					// Download the SQL backup file to the browser
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
					header('Content-Transfer-Encoding: binary');
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					header('Content-Length: ' . filesize($backup_file_name));
					ob_clean();
					flush();
					readfile($backup_file_name);
					exec('rm ' . $backup_file_name); 
				}

                $this->load->view("admin/overview", $data);
            }
    }
    
}