<?php

class Login extends CI_Controller {

    public function __construct(){

            parent::__construct();
                $this->load->helper('url');
                $this->load->model('M_user');
                $this->load->model('M_sekolah');
                $this->load->model('M_customer');
                $this->load->model('m_kabupaten');
                $this->load->library('Session');
                $this->load->library('email');

    }

    public function index(){
        $data['kabupaten'] = $this->M_sekolah->get_all_kabupaten();
        $data['provinsi'] = $this->M_sekolah->get_all_provinsi();
        $data["kabupaten"] = $this->m_kabupaten->getAll();
        $data['level'] = $this->M_sekolah->level_customer();
        $this->load->view("customer/login/v_register_customer", $data);
    }

    public function register_customer(){
            $this->form_validation->set_rules('email', 'Email', 'valid_email|required');
            $nama_sekolah = $this->input->post('xnama');
            $alamat_customer = $this->input->post('xalamat');
            $email_sekolah = $this->input->post('xemail');
            $no_hp = $this->input->post('xnomerhp');
            $id_kabupaten = $this->input->post('xidkabupaten');
            $password_customer = md5($this->input->post('xpassword'));
            $password = $this->input->post('xpassword');
            $level = $this->input->post('xidlevel');
            // $user=array(
            // 'nama_sekolah'=>$this->input->post('user_name'),
            // 'email_sekolah'=>$this->input->post('user_email'),
            // 'no_hp'=>$this->input->post('number_phone'),
            // 'password_customer'=>md5($this->input->post('user_password')),
            //     );
                //print_r($user);
            // $email_check=$this->M_user->email_check($user['user_email']);
        $email_check=$this->M_customer->email_check($email_sekolah);
        if($email_check){
        // $this->M_user->register_user($user);

        
        // $this->M_sekolah->tambah_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer);
        // $id_pelanggan=$this->M_sekolah->ambil_id($email_sekolah);
        // $this->M_customer->update_id($email_sekolah,$id_pelanggan);
        $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $code = substr(str_shuffle($set), 0, 12);
        
        $this->M_customer->register_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer, $code, $level);
        $id = $this->M_customer->ambil_id($email_sekolah);
        $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
        // testing
        $message = 	"
        <html>
        <head>
            <title>Verification Code</title>
        </head>
        <body>
            <h2>Aktivasi Akun</h2>
            <p>Akun anda sudah terdaftar.</p>
            <h4><a href='".base_url()."customer/login/activate/".$id."/".$code."'>Aktifkan Akun Saya</a></h4>
        </body>
        </html>
        ";
        $ci = get_instance();
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.googlemail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "qwertyzxcvbn081@gmail.com";
        $config['smtp_pass'] = "yangbukaemailiniselaingemaantikaharam";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $ci->email->initialize($config);
        $ci->email->from('cs.sibukismuba@sibukismuba.com', 'Ismuba');
        $list = array($email_sekolah);
        $ci->email->to($list);
        $ci->email->subject('Verifikasi Akun Ismuba');
        $ci->email->message($message);
        if ($this->email->send()) {
            $this->session->set_flashdata('emailterkirim','Kode aktifasi terkirim ke email anda');
        } else {
        show_error($this->email->print_debugger());
        }
                	redirect('customer/login/login_view');
        }else{
            $this->session->set_flashdata('error_msg', 'Email Sudah Terdaftar');
    		redirect('customer/login/login_view');
        }
        //testing

        //set up email
		// 	$config = array(
        //         'protocol' => 'smtp',
        //         'smtp_host' => 'ssl://smtp.googlemail.com',
        //         'smtp_port' => 465,
        //         'smtp_user' => 'qwertyzxcvbn081@gmail.com', // change it to yours
        //         'smtp_pass' => 'yangbukaemailiniselaingemaantikaharam', // change it to yours
        //         'mailtype' => 'html',
        //         'charset' => 'iso-8859-1',
        //         'wordwrap' => TRUE
        //   );
        //   $message = 	"
        //               <html>
        //               <head>
        //                   <title>Verification Code</title>
        //               </head>
        //               <body>
        //                   <h2>Terimakasih Telah Mendaftar di ISMUBA</h2>
        //                   <p>Akun Anda:</p>
        //                   <p>E-mail  : ".$email_sekolah."</p>
        //                   <p>Password : ".$password."</p>
        //                   <p>Klik Disini untuk aktivasi akun</p>
        //                   <h4><a href='".base_url()."customer/login/activate/".$id."/".$code."'>Aktifkan Akun Saya</a></h4>
        //               </body>
        //               </html>
        //               ";
           
        //   $this->load->library('email', $config);
        //   $this->email->set_newline("\r\n");
        //   $this->email->from($config['smtp_user']);
        //   $this->email->to($email_sekolah);
        //   $this->email->subject('Signup Verification Email');
        //   $this->email->message($message);
        //   if($this->email->send()){
        //        $this->session->set_flashdata('emailterkirim','Kode verifikasi terkirim ke email ');
        //     }
        //     else{
		//     	$this->session->set_flashdata('emaileror', $this->email->print_debugger());
		//     }
        // redirect('customer/login/login_view');
        // }
        // else{
        // $this->session->set_flashdata('error_msg', 'Email Sudah Terdaftar');
        // redirect('customer/login');
        // }   

    }

    public function login_view(){
        $this->load->view("customer/login/v_login_customer");

    }

    function login_user_customer(){
        $user_login=array(

        'email_sekolah'=>$this->input->post('user_email'),
        'password_customer'=>md5($this->input->post('user_password'))

            );

            $data=$this->M_customer->login_user_costomer($user_login['email_sekolah'],$user_login['password_customer']);
            if($data)
            {
                $status = $this->M_customer->ambil_status($user_login['email_sekolah']);
                if($status != 1){
                    $this->session->set_flashdata('error_msg', 'Akun Belum teraktifasi');
                    $this->load->view("customer/login/v_login_customer");
                }else{
                    $this->session->set_userdata('id_sekolah',$data['id_sekolah']);
                    $this->session->set_userdata('email_sekolah',$data['email_sekolah']);
                    $this->session->set_userdata('nama_sekolah',$data['nama_sekolah']);
                // <?php echo $this->session->userdata('nama_sekolah'); buat echo doang biar inget
                    redirect('customer');
                }
            }
            else{
                $this->session->set_flashdata('error_msg', 'Kombinasi email dan password salah.!');
                $this->load->view("customer/login/v_login_customer");

            }


    }

    function user_profile(){

        $this->load->view('admin/user/v_user_profile.php');
        }

    public function user_logout_hiya(){
        $this->session->sess_destroy();
        redirect('customer', 'refresh');
    }

    public function activate($id, $code){	
		$id =  $this->uri->segment(4);
		$code = $this->uri->segment(5);	
		//fetch user details
		$user = $this->M_customer->getUser($id);

		//if code matches
		if($user['code'] == $code){
			//update user active status
			// $data['active'] = true;
			$query = $this->M_customer->activin($id);
			if($query){
				$this->session->set_flashdata('pesanaktifasi', 'Akun berhasil di aktifkan, silahkan Masuk.!');
			}
			else{
				$this->session->set_flashdata('pesanaktifasi', 'Something went wrong in activating account');
			}
		}
		else{
			$this->session->set_flashdata('pesanaktifasi', 'Cannot activate account. Code didnt match');
		}
		redirect('customer/login/login_view');
    }
    
    public function lupa_password(){
        $this->load->view('customer/login/v_reset_password');
    }
    public function reset_password(){
        $email_sekolah = $this->input->post('xemail');
        $email_check=$this->M_customer->email_checkpass($email_sekolah);
        if($email_check != null){
            $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $code = substr(str_shuffle($set), 0, 12);
            
            $id = $this->M_customer->ambil_id($email_sekolah);
            // block sampe bawah
                
            $message = 	"
                        <html>
                        <head>
                            <title>Reset Password ISMUBA</title>
                        </head>
                        <body>
                            <h6>Lain kali di ingat ya passwordnya :') </h6>
                            <p>Klik Disini untuk reset password</p>
                            <h4><a href='".base_url()."customer/login/update_password/".$id."'>Ubah password akun ISMUBA</a></h4>
                        </body>
                        </html>
                        ";
            
            $ci = get_instance();
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://smtp.googlemail.com";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "qwertyzxcvbn081@gmail.com";
                $config['smtp_pass'] = "yangbukaemailiniselaingemaantikaharam";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $ci->email->initialize($config);
                $ci->email->from('qwertyzxcvbn081@gmail.com', 'Ismuba');
                $list = array($email_sekolah);
                $ci->email->to($list);
                $ci->email->subject('Reset Password Ismuba');
                $ci->email->message($message);
            if($this->email->send()){
                $this->session->set_flashdata('linkpassword','Link ubah password terkirim ke email anda');
                }
                else{
                    $this->session->set_flashdata('emaileror', $this->email->print_debugger());
                }
            redirect('customer/login/login_view');
        }
        else{
                echo "email tidak terdaftar ".$email_sekolah;
        }
        // block sampe bawah
    }

    
    function update_password($id){
        $data['email_sekolah'] = $this->M_customer->ambil_email($id);
        $data['id_sekolah'] = $id;
        $this->load->view('customer/login/v_form_ubah_password.php', $data);
    }

    function password_baru(){
        $id = $this->input->post('xid');
        $email_sekolah = $this->input->post('xemail');
        $password = md5($this->input->post('xpassword'));

        $query = $this->M_customer->passworbaru($id, $email_sekolah, $password);
        if($query){
            $this->session->set_flashdata('newpassword', 'Ubah Password Berhasil');
            redirect('customer/login/login_view');
        }else{
            $this->session->set_flashdata('newpassword', 'Ganti Password Gagal');
            redirect('customer/login/login_view');
        }
    }

}

?>