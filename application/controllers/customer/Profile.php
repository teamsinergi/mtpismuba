<?php 

class Profile extends CI_Controller{

    public function __construct(){ 
            parent::__construct();
            $this->load->model('m_sekolah');
            $this->load->helper('url');
    }

	public function index(){
            $data['profile']=$this->m_sekolah->view_profile();
            $data['nama_sekolah'] = $this->m_sekolah->nama_pelanggan();
            // $this->load->view('customer/profile/v_profile', $data); 
            $this->load->view('customer/profile/v_profile_v2', $data);   
    }

    
	public function edit_profile(){
            $email = $this->session->userdata('email_sekolah');
            $data["kabupaten"] = $this->m_sekolah->kabupaten($email);
            $data['provinsi'] = $this->m_sekolah->get_all_provinsi();
            $data['profile']=$this->m_sekolah->view_profile();
            $data['nama_pelanggan'] = $this->m_sekolah->nama_pelanggan();
            $this->load->view('customer/profile/v_edit_profile', $data);    
        }

    public function update_profile(){
        $nama = $this->input->post("xnama");
        $no = $this->input->post("xno");
        $alamat = $this->input->post("xalamat");
        $email = $this->input->post("xemail");
        $password = md5($this->input->post("xpassword"));
        $kabupaten = $this->input->post('xidkabupaten');
        $this->m_sekolah->update_user($nama, $no, $alamat, $email, $password, $kabupaten);
        redirect("customer/profile");
    }
}