<?php 


class Riwayat extends CI_Controller{
        
    public function __construct(){ 
        parent::__construct();
        $this->load->model('m_sekolah');
        $this->load->model('m_customer');
        $this->load->model('m_transaksi');
        $this->load->model('m_cart');
        $this->load->helper('url');
        $this->load->library('upload');
    }

    public function index()
	{
        $data['riwayat'] = $this->m_customer->riwayat_customer();
        $data['proses'] = $this->m_customer->riwayat_proses();
        $this->load->view("customer/riwayat/v_tampil_riwayat", $data);
    }

    public function rincian($kode_transaksi){
        $data['detailtransaksi']=$this->m_cart->detail_transaksi($kode_transaksi);
        $data['totalharga']=$this->m_cart->detail_harga($kode_transaksi);
        $data['kode'] = $kode_transaksi;
        $data['status_barang'] = $this->m_cart->status_barang($kode_transaksi);
        $data['kode_status'] = $this->m_cart->kode_status($kode_transaksi);
        $data['cekbukti'] = $this->m_transaksi->cek_bukti_transfer($kode_transaksi);
        $this->load->view("customer/riwayat/v_tampil_rincian", $data);
    }

    public function status_diterima($kode){
        $this->m_transaksi->status_diterima($kode);
        redirect('customer/riwayat/rincian/'.$kode);
    }

    public function rincian_pesanan($kode_transaksi){
        $data['detailtransaksi']=$this->m_cart->detail_transaksi_pesanan($kode_transaksi);
        $data['totalharga']=$this->m_cart->detail_harga_pesanan($kode_transaksi);
        $data['kode'] = $kode_transaksi;
        //$data['status_barang'] = $this->m_cart->status_barang($kode_transaksi);
        $data['cekbukti'] = $this->m_transaksi->cek_bukti_transfer($kode_transaksi);
        $this->load->view("customer/riwayat/v_tampil_rincian_pesanan", $data);
    }

    public function simpan_bukti(){

        $config['upload_path'] = './assets/images/bukti'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['encrypt_name'] = TRUE; //nama yang terupload nantinya


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                $gbr = $this->upload->data();
                $config['image_library']='gd2';
                $config['source_image']='./assets/images/bukti'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= FALSE;
                $config['quality']= '50%';
                // $config['width']= 300;
                // $config['height']= 300;
                $config['new_image']= './assets/images/bukti'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $photo=$gbr['file_name'];
                $kode = $this->input->post("xkode");
                $query = $this->m_cart->simpan_bukti($kode, $photo);
                if($query){
                    $this->session->set_flashdata('pesanbukti', 'Bukti Transfer Berhasil Terkirim.!');
                }
                else{
                    $this->session->set_flashdata('pesanbukti', 'Bukti Transfer Gagal Terkirim.!');
                }
                
                redirect('customer/riwayat');
            }
        }else{
            redirect('customer/riwayat');
        }
    }

    function cetak_nota($kode){
        $id_sekolah = $this->session->userdata('id_sekolah');
        $data['detailtransaksi']=$this->m_transaksi->detail_transaksi($id_sekolah, $kode);
		$data['totalharga']=$this->m_transaksi->detail_harga($id_sekolah, $kode);
        $data['pelanggan']=$this->m_transaksi->nama_sekolah($id_sekolah);
		$data['tanggalnota'] = $this->m_transaksi->tanggal_transaksi($kode);
		$data['idsekolahnota'] = $id_sekolah;
		$data['kodetransaksi'] = $kode;
		$data['tanggalprint'] = date('d/M/Y');
		$data['alamatpengiriman'] = $this->m_transaksi->alamat_pengiriman($kode);
	
        $this->load->view('customer/riwayat/v_cetak_nota',$data);
    }

}