<?php 


class Cart extends CI_Controller{

	public function __construct() 
        { 
            parent::__construct();
            $this->load->model('m_buku');
            $this->load->model('m_cart');
            $this->load->model('m_transaksi');
            $this->load->helper('url');
            $this->load->library('session'); 
        }
	
	public function index()
	{
        if($this->session->userdata('nama_sekolah')==null){
            redirect('customer/login');
        }else{
            $data['tampung'] = $this->m_cart->tampil_tampung_buku();
            $data['buku'] = $this->m_buku->getAll();
            $data['cekbuku'] = $this->m_buku->cek_buku();
            $this->load->view('customer/cart/v_cart',$data);    
        }
    }

    public function tambah_barang($idbarang){
        if($this->session->userdata('nama_sekolah')==null){
            redirect('customer/login/login_view');
        }else{
            $query = $this->m_cart->tampung_buku($idbarang);
            if($query){
                $this->session->set_flashdata('pesan','Berhasil di tambah ke keranjang');
                $data['detailbuku'] = $this->m_buku->detail_buku($idbarang);
                $this->load->view("customer/detail/v_detail", $data);
            }
        }
    }

    public function simpan_update_transaksi(){

		$idbarang = $this->input->post('xbarang');
		$banyakbarang = $this->input->post('xbanyak');
        $jumlah = count($idbarang);
        for($x=0;$x<$jumlah;$x++){
            $harga = $this->m_transaksi->ambil_harga($idbarang[$x]);
            $cekstok = $this->m_buku->cek_stok($idbarang[$x]);
            if($banyakbarang[$x]<1 || $banyakbarang[$x] > $cekstok){
                redirect('customer/cart');
            }else{
                $this->m_cart->update_cart($idbarang[$x], $banyakbarang[$x], $harga);
            }

        }
        redirect('customer/cart/lanjutan');
    }

    public function hapusbuku($idbuku){
        $this->m_cart->hapusbuku($idbuku);
        redirect('customer/cart');
    }

    public function lanjutan(){
        $data['tampung'] = $this->m_cart->tampil_tampung_buku();
        $data['buku'] = $this->m_buku->getAll();
        $data['carttotal'] = $this->m_cart->cart_total();
        $data['alamat'] = $this->m_cart->alamat_customer();
        $this->load->view('customer/cart/v_cart_konfirmasi',$data);
    }

    public function simpan_konfirmasi_transaksi(){
        $idbarang = $this->input->post('xbarang');
        $banyakbarang = $this->input->post('xjumlah');
        $total = $this->input->post('xtotal');
        $idpembeli = $this->input->post('xidpembeli');
        $jumlah = count($idbarang);
        $tujuan = $this->input->post("xtujuan");
        for($x=0;$x<$jumlah;$x++){
			// $this->m_transaksi->simpan_transaksi($idsekolah, $idbarang[$x], $banyakbarang[$y], $harga);
            // $this->m_cart->simpan_konfirmasi_transaksi($idbarang[$x], $banyakbarang[$x], $total[$x], $idpembeli);
            $this->m_cart->kirim_pesanan($idbarang[$x], $banyakbarang[$x], $total[$x], $idpembeli, $tujuan);
            $this->m_cart->delete_pesanan_customer();
        }
        redirect('customer/riwayat');
    }

}

?>