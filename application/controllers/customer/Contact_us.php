<?php 


class Contact_us extends CI_Controller{
    public function __construct(){

        parent::__construct();
            $this->load->helper('url');
            $this->load->model('m_komentar');
            $this->load->library('Session');
}
    public function index()
    {
        $this->load->view("customer/contact_us/v_contact_us");
    }

    public function simpan_pesan(){
        $nama = $this->input->post('xnama');
        $email = $this->input->post('xemail');
        $komentar = $this->input->post('xpesan');
        $nomer = $this->input->post('xno');
        $this->m_komentar->simpankomentar($email,$komentar, $nomer, $nama);
        redirect('customer','refresh');
    }

    
}

?>