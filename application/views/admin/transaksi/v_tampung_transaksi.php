<!DOCTYPE html>
<html lang="en">

<head>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/images'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">
				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>
				
				<!-- DataTables -->
						<form action="<?php echo base_url().'admin/transaksi/simpan_transaksi_tampung'?>" method="post" enctype="multipart/form-data" >
                            <table class="table table" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nomer</th>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Jumlah Barang</th>
										<th>Harga</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=0; foreach($detailpesanan as $data): $no++?>
									<tr>
										<td><?php echo $no?></td>
										<td> <?php echo $data->nama_buku." Kelas ".$data->kelas_buku?><div class="product_id" hidden><a href="product.html"><input type="text" name="xbarang[]" value="<?php echo $data->id_buku?>"></a></div></td>
										<td><?php echo number_format($data->harga_buku,0,',','.')?></td>
										<td> <?php echo number_format($data->banyak_barang,0,',','.')?><div class="product_id" hidden><a href="product.html"><input type="text" name="xbanyak[]" value="<?php echo $data->banyak_barang?>"></a></div></td>
										<td> <?php echo number_format($data->total_harga,0,',','.')?><div class="product_id" hidden><a href="product.html"><input type="text" name="xtotal[]" value="<?php echo $data->total_harga?>"></a></div></td>
                                        <td hidden> <?php echo $data->id_sekolah?><div class="product_id" hidden><a href="product.html"><input type="text" name="xcustomer" value="<?php echo $data->id_sekolah?>"></a></div></td>
                                        <td hidden> <?php echo $data->kode_transaksi?><div class="product_id" hidden><a href="product.html"><input type="text" name="xidtransaksi" value="<?php echo $data->kode_transaksi?>"></a></div></td>
									</tr>
									<?php endforeach?>
									<tr>
										<td colspan=4>Total</td><td ><input type="text" class="form-control" value="<?php echo $totalharga?>" readonly></td>
									</tr>
									<tr>
										<td>Alamat</td><td colspan=4><input type="text" class="form-control" name="xtujuan" value="<?php echo $alamat?>"></td>
									</tr>
								</tbody>
								</table>
								<input style="margin-bottom: 13.67%; margin-left: -24%; width: 80%" class="btn btn-info" type="submit" name="btn" value="KONFIRMASI PESANAN" /> 
								<br>
								<a style="margin-top: 5.7%; width: 96.23%; margin-left: 2.6%" href="<?php echo site_url('admin/transaksi/batal')?>" class="btn btn-danger">BATAL</a>
							</form>
						</div>
					</div>
				</div>
			</div>


			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
