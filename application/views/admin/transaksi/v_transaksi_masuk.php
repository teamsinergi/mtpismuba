<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body id="page-top">

<?php $this->load->view("admin/_partials/navbar.php") ?>

<div id="wrapper" >

	<?php $this->load->view("admin/_partials/sidebar.php") ?>

	
	<div id="content-wrapper" >
		<div class="container-fluid" >

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>
<div class="card mb-3">
<div class="card-body">
		<div class="row">
			<?php foreach($pesanan as $data):?>
			<div class="col-xl-4 col-sm-6 mb-3" >
				<div class="card text-white bg-primary o-hidden h-100" >
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-shopping-cart"></i>
						</div>
						<h4>Pesanan dari <?php echo $data->email_pembeli?></h4>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('admin/transaksi/masuk_detail/'.$data->id_pembeli.'/'.$data->id_transaksi)?>">
						<span class="float-left">View Details</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>

		<?php endforeach?>
		</div>
		</div>
</div>
		</div>
		</div>
		<!-- /.container-fluid -->

		<!-- Sticky Footer -->
		<?php $this->load->view("admin/_partials/footer.php") ?>

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
