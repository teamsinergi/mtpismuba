<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/buku/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

					<form action="<?php echo base_url().'admin/transaksi/tampung_transaksi'?>" method="post" enctype="multipart/form-data" >
							<div class="form-group">
								<label for="name"><h6>Pelanggan*</h6></label>
								<div class="form-group">
                                        <select class="form-control" id="sel1" name="xsekolah">
                                        <?php 
                                            foreach ($sekolah as $data):
                                        ?>
                                            <option value="<?php echo $data->id_sekolah;?>"><?php echo $data->nama_sekolah?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
							</div>
					<div id="accordion">
					<h6>Pilih Buku*</h6>
						<div class="card">
						<div class="card-header">
							<a class="card-link" data-toggle="collapse" href="#collapseOne">
							Buku SD/MI
							</a>
						</div>
						<div id="collapseOne" class="collapse show" data-parent="#accordion">
							<div class="card-body">
							<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
											<tr>
												<th width="200">Judul Buku*</th>
												<th width="100">Kelas*</th>
												<th width="100">Harga*</th>
												<th>Stok</th>
												<th>Banyak*</th>
											</tr>
											<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
											
												<?php foreach($buku as $data):?>
												<?php if($data->kelas_buku < 7 && $data->kelas_buku != "-"):?>
												<tr>
													<td width="200"><input type="checkbox" class="form-check-input" id="check1" name="xbarang[]" alt="Checkbox" value="<?php echo $data->id_buku?>" ><?php echo $data->nama_buku?></td>
													<td width="100">Kelas <?php echo $data->kelas_buku?></td>
													<td width="100"><?php echo number_format($data->harga_buku,0,',','.')?></td>
													<td><?php echo number_format($data->stok,0,',','.')?></td>
													<td>
													<input class="form-control col-sm-4" type="number" name="xbanyak[]"/>
													</td>
												</tr>
												<?php endif?>
												<?php endforeach;?>
											</table>
							</table>
							</div>
						</div>
						</div>
						<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
							Buku SMP/MTS
						</a>
						</div>
						<div id="collapseTwo" class="collapse" data-parent="#accordion">
							<div class="card-body">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
												<tr>
												<th width="200">Judul Buku*</th>
												<th width="100">Kelas*</th>
												<th width="100">Harga*</th>
												<th>Stok</th>
												<th>Banyak*</th>
											</tr>
												<?php foreach($buku as $data) :?>
												<?php if($data->kelas_buku < 10 && $data->kelas_buku > 6):?>
												<tr>
													<td width="200"><input type="checkbox" class="form-check-input" id="check1" name="xbarang[]" alt="Checkbox" value="<?php echo $data->id_buku?>" ><?php echo $data->nama_buku?></td>
													<td width="100">Kelas <?php echo $data->kelas_buku?></td>
													<td width="100"><?php echo number_format($data->harga_buku,0,',','.')?></td>
													<td><?php echo number_format($data->stok,0,',','.')?></td>
													<td>
													<input class="form-control col-sm-4" type="number" name="xbanyak[]"/>
													</td>
												</tr>
												<?php endif?>
												<?php endforeach;?>
											</table>
								</table>
							</div>
						</div>
						</div>
						<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
							Buku SMA/MA/SMK
							</a>
						</div>
						<div id="collapseThree" class="collapse" data-parent="#accordion">
							<div class="card-body">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
												<tr>
												<th width="200">Judul Buku*</th>
												<th width="100">Kelas*</th>
												<th width="100">Harga*</th>
												<th>Stok</th>
												<th>Banyak*</th>
											</tr>
												<?php foreach($buku as $data) :?>
												<?php if($data->kelas_buku > 9):?>
												<tr>
													<td width="200"><input type="checkbox" class="form-check-input" id="check1" name="xbarang[]" alt="Checkbox" value="<?php echo $data->id_buku?>" ><?php echo $data->nama_buku?></td>
													<td width="100">Kelas <?php echo $data->kelas_buku?></td>
													<td width="100"><?php echo number_format($data->harga_buku,0,',','.')?></td>
													<td><?php echo number_format($data->stok,0,',','.')?></td>
													<td>
													<input class="form-control col-sm-4" type="number" name="xbanyak[]"/>
													</td>
												</tr>
												<?php endif?>
												<?php endforeach;?>
											</table>
											</table>
							</div>
						</div>
						</div>
						<div class="card">
						<div class="card-header">
							<a class="collapsed card-link" data-toggle="collapse" href="#collapseThree1">
							Buku Umum
							</a>
						</div>
						<div id="collapseThree1" class="collapse" data-parent="#accordion">
							<div class="card-body">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
												<tr>
												<th width="200">Judul Buku*</th>
												<th width="100">Kelas*</th>
												<th width="100">Harga*</th>
												<th>Stok</th>
												<th>Banyak*</th>
											</tr>
												<?php foreach($buku as $data) :?>
												<?php if($data->kelas_buku == "-"):?>
												<tr>
													<td width="200"><input type="checkbox" class="form-check-input" id="check1" name="xbarang[]" alt="Checkbox" value="<?php echo $data->id_buku?>" ><?php echo $data->nama_buku?></td>
													<td width="100">Kelas <?php echo $data->kelas_buku?></td>
													<td width="100"><?php echo number_format($data->harga_buku,0,',','.')?></td>
													<td><?php echo number_format($data->stok,0,',','.')?></td>
													<td>
													<input class="form-control col-sm-4" type="number" name="xbanyak[]"/>
													</td>
												</tr>
												<?php endif?>
												<?php endforeach;?>
											</table>
											</table>
							</div>
						</div>
						</div>
					</div>
					<br>
					<input class="btn btn-success col-12" type="submit" name="btn" value="Simpan" />                        
				</form>

					</div>

					<div class="card-footer small text-muted">
						* Isi Semua Data
					</div>


				</div>
				
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->
			
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>