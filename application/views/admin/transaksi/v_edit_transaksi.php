<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
                    </div>
                    <?php foreach ($edit_transaksi as $data):?>
						<a href="<?php echo site_url('admin/transaksi') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					<div class="card-body">

						<form action="<?php echo base_url().'admin/transaksi/update_transaksi'?>" method="post" enctype="multipart/form-data">
							<!-- <div class="form-group" hidden>
								<label for="name">idbuku*</label>
								<input class="form-control" type="text" name="xid" value="<?php echo $data->id_transaksi?>"/>
                            </div>  -->
                            <div class="form-group" hidden>
								<label for="id">id transaksi*</label>
								<input class="form-control" type="text" name="xid" value="<?php echo $data->id_transaksi?>" />
                            </div>
							<div class="form-group">
								<label for="name">Nama Buku*</label>
								<input class="form-control" type="text" name="xnama" value="<?php echo $data->nama_buku?>" readonly/>
                            </div>

							<div class="form-group">
								<label for="name">Banyak Barang*</label>
								<input class="form-control" type="number" name="xbanyak" value="<?php echo $data->banyak_barang?>" />
                            </div>
                            
							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>
                    <?php endforeach; ?>
					<div class="card-footer small text-muted">
						* Isi Semua Data
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
