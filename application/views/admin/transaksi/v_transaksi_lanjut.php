<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
						<strong><?php echo $pelanggan?></strong>
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nomer*</th>
										<th>Nama Barang*</th>
                                        <th>Kelas*</th>
                                        <th>Harga*</th>
										<th>Banyak*</th>
										<th>Total Harga*</th>
										<!-- <th>Action*</th> -->
									</tr>
								</thead>
								<tbody>
                                    <?php
                                    $no = 0; 
                                    foreach ($detailtransaksi as $data):
                                    $no++; ?>
									<tr>
										<td>
											<?php echo $no?>
										</td>
										<td>
											<?php echo $data->nama_buku ?>
										</td>
                                        <td>
                                            <?php echo "Kelas ".$data->kelas_buku?>
                                        </td>
                                        <td>
                                            <?php echo number_format($data->harga_buku,0,',','.')?>
                                        </td>
										<td>
											<?php echo number_format($data->banyak_barang,0,',','.')?>
										</td>
										<td>
											Rp. <?php echo number_format($data->total_harga,0,',','.')?>
										</td>
										<!-- <td width="250">
											<a href="<?php echo site_url('admin/transaksi/edit/'.$data->id_transaksi) ?>"
											 class="btn btn-small"><i class="fas fa-edit"></i> Edit</a>
											<a href="<?php echo site_url('admin/transaksi/hapus_tranasksi/'.$data->id_transaksi.'/'.$data->id_sekolah) ?>"
											 class="btn btn-small text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td> -->
									</tr>
                                    <?php endforeach; ?>
                                    <?php 
                                        foreach ($totalharga as $total):
                                    ?>
                                    <tr>
                                        <th colspan='5'>TOTAL HARGA</th><th colspan=2>Rp. <?php echo number_format($total->totalharga,0,',','.')?></th>
                                    </tr>
                                        <?php endforeach;?>
								</tbody>
							</table>
							<a class="btn btn-outline-primary"  href="<?php echo site_url('admin/transaksi/cetak_nota/'.$idsekolahnota."/".$tanggalnota."/".$kode)?>" role="button">Cetak Nota</a>
							<a data-toggle="modal" data-target="#ModalHapus<?php echo $kode?>"
							class="btn btn-outline-primary"><i class="fas fa-location-arrow"></i> Status</a>
						</div>
					</div>
                    
				</div>

			</div>

		<?php foreach ($transaksi as $data):?>
                <!--Modal hapus buku-->
				<div class="modal fade" id="ModalHapus<?php echo $data->kode_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="myModalLabel">Edit status</h4>
							</div>
							<form class="form-horizontal" action="<?php echo base_url().'admin/transaksi/status_barang'?>" method="post" enctype="multipart/form-data">
							<div class="modal-body">

							<div class="form-group">
								<label for="name">Status Barang*</label>
                                    <div class="form-group">
                                        <select class="form-control" id="sel1" name="xstatus" required>
                                        <?php 
                                            foreach ($status as $data):
                                        ?>
                                            <option value="<?php echo $data->id_status;?>"><?php echo $data->nama?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                            </div>

							<div class="form-group" hidden>
								<label for="name">Status Barang*</label>
                                    <div class="form-group">
                                        <select class="form-control" id="sel1" name="xkode" required>
                                            <option value="<?php echo $kode;?>"><?php echo $kode?></option>
                                        </select>
                                    </div> 
                            </div>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-info btn-flat" id="simpan">Update</button>
							</div>
							</form>
						</div>
					</div>
				</div>
   		 <?php endforeach; ?>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
