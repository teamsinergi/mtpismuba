<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">
	<div id="wrapper">
		<div id="content-wrapper">
			<div class="container-fluid">
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
					<center><h4>Nota Belanja ISMUBA</h4></center>
					<center><h6>Jl. Gedongkuning No.131, Rejowinangun, Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55171</h6></center>
					</div>
					<table>
						<tr>
							<td>Kode Transaksi</td><td>:</td><td><?php echo $kodetransaksi?></td>
						</tr>
						<tr>
							<td>Nama Pelanggan</td><td>:</td><td><?php echo $pelanggan?></td>
						</tr>
						<tr>
							<td>Tanggal Transaksi</td><td>:</td><td><?php echo $tanggalnota?></td>
						</tr>
						<tr>
							<td>Alamat Pengiriman</td><td>:</td><td><?php echo $alamatpengiriman?></td>
						</tr>
					</table>
					<div class="card-body">
						<div>
							<table border=1 width="100%" cellspacing="0">
								<thead>
									<tr style="text-align: center;">
										<th>Nomer*</th>
										<th>Nama Barang*</th>
                                        <th>Kelas*</th>
                                        <th>Harga*</th>
										<th>Banyak*</th>
										<th>Total Harga*</th>
									</tr>
								</thead>
								<tbody style="text-align: center;">
                                    <?php
                                    $no = 0; 
                                    foreach ($detailtransaksi as $data):
                                    $no++; ?>
									<tr>
										<td>
											<?php echo $no?>
										</td>
										<td>
											<?php echo $data->nama_buku ?>
										</td>
                                        <td>
                                            <?php echo "Kelas ".$data->kelas_buku?>
                                        </td>
                                        <td>
                                            <?php echo $data->harga_buku?>
                                        </td>
										<td>
											<?php echo $data->banyak_barang?>
										</td>
										<td>
											Rp. <?php echo number_format($data->total_harga,0,',','.')?>
										</td>
									</tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th colspan='5'>TOTAL HARGA</th><th colspan=2>Rp. <?php echo number_format($totalharga,0,',','.')?></th>
                                    </tr>
								</tbody>
							</table>
							<br>
							<div class="container">
								<div class="row">
									<p><?php echo $tanggalprint?></p>
								</div>
								<div class="row">
								<hr>
									<p>.</p>
								</div>
								<div class="row">
									<hr>
									<p>.</p>
								</div>
								<div class="row">
									
									<p>Lalu Muhammad Brian Akbar alpino</p>
								</div>
							</div>
						</div>
					</div>
                    
				</div>

			</div>

		
		</div>
	</div>
</body>

</html>
