<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
						Komentar Netizen
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-striped" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Email</th>
										<th>Komentar</th>
										<th>Nomer hp</th>
										<th>Tanggal</th>
										<th>Options</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 0; 
										foreach ($komentar as $data):
										$no++; ?>
									<tr>
										<td width="10">
											<?php echo $no ?> 
										</td>
										<td>
											<?php echo $data->nama ?>
										</td>
										<td width="20">
											<?php echo $data->email ?>
										</td>
										<td>
											<?php echo $data->komentar ?>
										</td>
										<td>
											<?php echo $data->no_hp ?>
										</td>
										<td>
											<?php echo $data->tanggal ?>
										</td>
										<td>
											<a href="<?php echo site_url('admin/contact_us/hapus/'.$data->id_komentar)?>" class="btn btn-small text-danger"><i class="fas fa-trash"></i></a>
										</td>

									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>

	<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
