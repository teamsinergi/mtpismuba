<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<div id="wrapper">
		<div id="content-wrapper">

			<div class="container-fluid">
				<!-- DataTables -->
				<div class="row">
                    <div class="col-xl-6 col-sm-6 mb-3">
                        <!-- Card pertama-->
                        <div class="card text-black">
                            <div class="card-body">
                                <div class="container">
                                    <table>
                                        <tr>
                                        <?php foreach($totalbukuterjual_kab as $data): ?>
                                        <h3>Kabaputen : <?php echo $data->nama_kabupaten?></h3>
                                        <?php endforeach?>
                                            <td>Total buku terjual</td>
                                            <?php foreach($totalbukuterjual_kab as $data):?>
                                            <td><?php echo number_format($data->totalbukuterjual,0,',','.')?></td>
                                            <?php endforeach;?>
                                        </tr>
                                        <tr>
                                            <td>Total pendapatan </td>
                                            <?php foreach($totalpendapatanpenjualan_kab as $data):?>
                                            <td width=25%><?php echo number_format($data->totalharga,0,',','.')?></td>
                                            <?php endforeach;?>
                                        </tr>
                                    </table>
                                </div>
                            </div>
		                </div>
                        <!-- tutup card pertama-->
        			</div>                    
			<!-- Sticky Footer -->
	    </div>
        <div class="container-fluid">
        <strong><i><h5>Daftar Pembeli</h5></i></strong>
            <div class="card text-black">
                <div class="card-body">
                    <div>
                                    <table width="100%" border="1">
                                        <thead>
                                            <tr style="text-align: center;">
                                                <th>Nomer*</th>
                                                <th>Nama Sekolah</th>
                                                <th>Nama buku</th>
                                                <th>Kelas</th>
                                                <th>Banyak Barang</th>
                                                <th>Total Barang</th>
                                                <th>Total Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=0; 
                                                    foreach($tampilsemua_kab as $data):
                                                        $no++;
                                                ?>
                                                <tr style="text-align: center;">
                                                    <td><?php echo $no?></td>
                                                    <td><?php echo $data->nama_sekolah?></td>
                                                    <td> 
                                                        <?php foreach($databuku as $key):?>
                                                        <?php if($data->id_sekolah==$key->id_sekolah){
                                                            echo $key->nama_buku."<br>";}?>
                                                        <?php endforeach?>                                                    
                                                    </td>
                                                    <td> 
                                                        <?php foreach($databuku as $key):?>
                                                        <?php if($data->id_sekolah==$key->id_sekolah){
                                                            echo " Kelas ".$key->kelas_buku."<br>";}?>
                                                        <?php endforeach?>                                                    
                                                    </td>
                                                    <td> 
                                                        <?php foreach($databuku as $key):?>
                                                        <?php if($data->id_sekolah==$key->id_sekolah){
                                                            echo $key->banyak_barang."<br>";}?>
                                                        <?php endforeach?>                                                    
                                                    </td>
                                                    <td><?php echo $data->banyak?></td>
                                                    <td>Rp. <?php echo number_format($data->totalharga,0,',','.')?></td>
                                                </tr>
                                                <?php endforeach; ?>
                                        </tbody>
                                    </table>
                    </div>
                </div>
            </div>
        </div>
		<!-- /.content-wrapper -->
	</div>
	<!-- /#wrapper -->
    <script type="text/javascript">
        window.print();
    </script>
</body>

</html>
