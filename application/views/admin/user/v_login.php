
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Masuk</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('aslog/images/icons/favicon.ico')?>"/>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/bootstrap/css/bootstrap.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/animate/animate.css')?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/css-hamburgers/hamburgers.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/select2/select2.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/util.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/main.css')?>">

</head>
<body>
  
  <div class="limiter">
    <div class="container-login100" ">
      
      <div class="wrap-login100" style="padding-right: 30px">
        <img src="<?php echo base_url('aslog/images/DIKDASMEN1.png')?>" width="100%">
        <br><br>
        <center>
          <div>
            <b style="color: #f6ff00">MAJLIS DISDAKMEN PUSAT MUHAMMADIYAH</b>
          </div>
        </center>
          
      </div>
      
        <center>
        <div style="width: 0px; height: 500px; border: 1px white solid;">
        </div>
        </center>

      
      <div class="wrap-login100 " style="padding-left: 30px; ">
        <form class="login100-form validate-form" role="form" method="post" action="<?php echo base_url('admin/user/login_user'); ?>">
          
          <span class="login100-form-title p-t-20 p-b-45">
            Masuk
          </span>

          <?php 
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');

                  if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                    <?php
                  }
                  ?>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
            <input type="text" class="input100" placeholder="Email" name="user_email" autofocus required>  
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
            <input type="password" class="input100" placeholder="Password" name="user_password" required>
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock"></i>
            </span>
          </div>

          <div class="container-login100-form-btn p-t-10">
            <button class="login100-form-btn">
              Masuk
            </button>
          </div>

          
          <div class="d-flex justify-content-center">
    
    </div>

          
        </form>
      </div>
      

    </div>
  </div>
  
  

  
  
  <script src="<?php echo base_url('aslog/vendor/jquery/jquery-3.2.1.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/popper.js')?>"></script>
  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/bootstrap.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/select2/select2.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/js/main.js')?>"></script>

</body>
</html>


<!-- Login Buku
<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php// $this->load->view("admin/user/csslog.php") ?>
  <title>Login</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3>Masuk</h3>
  </center>
   </div>
   <?php /*
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');

                  if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $error_msg; ?>
                    </div>
                    <?php
                  }
                  */?>

   <div class="card-body">
    <form role="form" method="post" action="<?php //echo base_url('admin/user/login_user'); ?>">
     <div >
       <font color="red"> <i class="fas fa-user"></i></font>
       <span><font color="white"> Email </font></span>
      </div>
     <div class="input-group form-group">
      <input type="text" class="form-control" placeholder="Email" name="user_email" required>     
     </div>
     <div >
       <font color="red"> <i class="fas fa-key"></i></font>
       <span><font color="white"> Password </font></span>
      </div>
     <div class="input-group form-group">
      <input type="password" class="form-control" placeholder="Password" name="user_password" required>
     </div>
     <?php /*<!-- <div class="row align-items-center remember">
      <input type="checkbox">Remember Me
     </div> -->*/?>
     <div class="form-group">
      <input type="submit" value="Masuk" class="btn float-right login_btn">
     </div>
    </form>
   </div>
   <div class="card-footer" hidden>
    <div class="d-flex justify-content-center links">
     Belum Punya Akun?<a href="<?php// echo base_url('admin/user'); ?>"><b>Daftar</b></a>
    </div>
   <?php /*<!-- <div class="d-flex justify-content-center">
    <a href="#">Forgot your password?</a> -->*/?>
    </div>
   </div>
  </div>
 </div>
</div>



</body>
</html>
-->