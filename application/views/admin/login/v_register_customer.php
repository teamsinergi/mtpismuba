<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("admin/login/csslogres.php") ?>
  <title>Register Akun</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
   <img width="20%" src="<?php echo base_url('assets2/images/dikdasmen.png')?>">
    <h3>Register</h3>
  </center>
   </div>
   <?php
                  $error_msg=$this->session->flashdata('error_msg');
                  if($error_msg){
                    echo $error_msg;
                  }
                   ?>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('customer/login/register_customer'); ?>">
     <fieldset>
                              <div class="form-group">
                                  <input class="form-control" placeholder="Username" name="user_name" type="text" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="password" name="user_password" type="password" autofocus>
                              </div>
                              <div class="form-group">
                                  <input class="form-control" placeholder="Nama Sekolah" name="nama_sekolah" type="text" value="">
                              </div>
                              <div class="form-group">
                              <textarea class="form-control" name="alamat" placeholder="Alamat"></textarea>
                              </div>
                              <div class="form-group">
                                  <input class="form-control" placeholder="E-mail" name="email" type="email" value="">
                              </div>


                             <div class="form-group">
      <input type="submit" value="Login" class="btn float-right login_btn">
     </div>

                          </fieldset>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links">
    <center><b>Already registered ?</b> <br></b><a href="<?php echo base_url('customer/login/login_view'); ?>">Login here</a></center><!--for centered text-->
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
