<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
	<script src="<?php echo base_url('assets/chartjs/Chart.js') ?>"></script>
</head>
<body id="page-top">

<?php $this->load->view("admin/_partials/navbar.php") ?>

<div id="wrapper">

	<?php $this->load->view("admin/_partials/sidebar.php") ?>

	<div id="content-wrapper">

		<div class="container-fluid">

        <!-- 
        karena ini halaman overview (home), kita matikan partial breadcrumb.
        Jika anda ingin mengampilkan breadcrumb di halaman overview,
        silahkan hilangkan komentar (//) di tag PHP di bawah.
        -->
		<?php //$this->load->view("admin/_partials/breadcrumb.php")
			
		?>

		<!-- Icon Cards-->
		<div class="row">
			<div class="col-xl-3 col-sm-6 mb-3">
				<div class="card text-white bg-primary o-hidden h-100">
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-book"></i>
						</div>
						<div class="mr-12">
							<?php foreach ($hitungbuku as $data): ?>
							Total Buku : <?php echo $data->banyak_buku?>
							<?php endforeach;?>
						</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('admin/buku1/')?>">
						<span class="float-left">Lihat Buku</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>
			
			<div class="col-xl-3 col-sm-6 mb-3">
				<div class="card text-white bg-warning o-hidden h-100">
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-list"></i>
						</div>
						<div class="mr-12">
							<?php foreach ($hitungpelanggan as $data): ?>
							Total Pelannggan : <?php echo $data->banyak_pelanggan?>
							<?php endforeach;?>
						</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('admin/sekolah/')?>">
						<span class="float-left">Lihat Pelanggan</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>
			<div class="col-xl-3 col-sm-6 mb-3">
			<div class="card text-white bg-success o-hidden h-100">
				<div class="card-body">
				<div class="card-body-icon">
					<i class="fas fa-fw fa-shopping-cart"></i>
				</div>
					<?php $count=0;
						foreach($total as $data): 
						$count++;
					?>
					<?php endforeach;?>
					Total Transaksi : <?php echo $count?>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('admin/transaksi/kecil')?>">
				<span class="float-left">Lihat Transaksi</span>
				<span class="float-right">
					<i class="fas fa-angle-right"></i>
				</span>
				</a>
			</div>
			</div>
			<div class="col-xl-3 col-sm-6 mb-3">
				<div class="card text-white bg-danger o-hidden h-100">
					<div class="card-body">
						<div class="card-body-icon">
							<i class="fas fa-fw fa-life-ring"></i>
						</div>
						<div class="mr-5">Laporan</div>
					</div>
					<a class="card-footer text-white clearfix small z-1" href="<?php echo site_url('admin/laporan/')?>">
						<span class="float-left">Lihat Laporan</span>
						<span class="float-right">
							<i class="fas fa-angle-right"></i>
						</span>
					</a>
				</div>
			</div>
		</div>
		<!-- Area Chart Example-->
		<hr>
		</div>
		<div class="container">
			<div class="row container">
			<div style="width:700px;margin: 0px auto;">
				<canvas id="myChart"></canvas>
			</div>
				<?php foreach($terlaris as $data):?>
						<div class="card" style="width:150px">
							<img class="card-img-top" <img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>" style="width:100%">
							<div class="card-body">
								<p style="font-size:10px">Buku Terlaris <br> <?php echo $data->nama_buku." Kelas ".$data->kelas_buku?> <br> <?php echo $data->stok_sisa?> Buku Terjual</p>
							</div>
						</div>
				<?php endforeach?>
				<?php foreach($sekolahterbanyak as $data):?>
				<div class="card" style="width:150px">
							<img class="card-img-top" <img src="<?php echo base_url('assets/images/mockup/terloyal.png') ?>" style="width:100%">
							<div class="card-body">
								<p style="font-size:10px">Pembeli Terbanyak <br> <?php echo $data->nama_sekolah?> <br><?php echo $data->banyak_barang?> Buku Terbeli</p>
							</div>
						</div>
				</div>
				<?php endforeach;?>

		</div>
		

		<!-- /.container-fluid -->

		<!-- Sticky Footer -->
		<?php $this->load->view("admin/_partials/footer.php") ?>

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<script>
		var ctx = document.getElementById("myChart").getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'bar',
			data: {
				labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
				datasets: [{
					label: '',
					data: [<?php $nilaigrafik = array(); 
			for($i=1; $i<12; $i++){
			$nilai = $this->m_laporan->ambil_nilai($i);
			$nilaigrafik[$i] = $nilai;
			echo $nilaigrafik[$i].",";
			}?>],
					backgroundColor: [
					'rgba(255, 99, 132, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
					'rgba(255, 99, 132, 0.2)',
                    'rgba(55, 172, 168, 0.2)',
					'rgba(54, 162, 235, 0.2)',
					'rgba(255, 206, 86, 0.2)',
					'rgba(75, 192, 192, 0.2)',
                    'rgba(55, 172, 168, 0.2)',
					'rgba(75, 192, 192, 0.2)',
                    'rgba(55, 172, 168, 0.2)',
					],
					borderColor: [
					'rgba(255,99,132,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(255,99,132,1)',
                    'rgba(55, 172, 168, 0.2)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
                    'rgba(55, 172, 168, 0.2)',
					'rgba(75, 192, 192, 1)',
                    'rgba(55, 172, 168, 0.2)',
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});
	</script>
<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
