<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script>
        $(document).ready(function(){
            $('#provinsi').change(function(){
                var provinsi_id = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('assets/kota.php')?>',
                    data: 'prov_id='+provinsi_id,
                    success: function(response){
                        $('#kota').html(response);
                    }
                });
            })
        });
    </script>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('admin/buku/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="<?php echo base_url().'admin/sekolah/simpan_sekolah'?>" method="post" enctype="multipart/form-data" >
							<div class="form-group">
								<label for="name">Nama Pelanggan*</label>
								<input class="form-control" type="text" name="xnamasekolah" placeholder="Nama Pelanggan" />
                            </div>
							<div class="form-group">
								<label for="name">Level*</label>
                                    <div class="form-group">
                                        <select class="form-control" id="sel1" name="xidlevel" required>
                                        <?php 
                                            foreach ($level as $data):
                                        ?>
                                            <option value="<?php echo $data->id_level;?>"><?php echo $data->nama_level?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                            </div>
							<div class="form-group">
								<label for="name">Email*</label>
								<input class="form-control" type="email" name="xemail" placeholder="Alamat Email" />
                            </div>

							<div class="form-group">
								<label for="name">Provinsi*</label>
								<div class="form-group">
									<select class="form-control" id="provinsi" name="xidkabupaten">
                                        <?php 
                                            foreach ($provinsi as $data):
                                        ?>
                                            <option value="<?php echo $data->id;?>"><?php echo $data->name?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                                </div>
								<div class="form-group">
								<label for="name">Kabupaten*</label>
								<div class="form-group">
									<select class="form-control" id="kota" name="xidkabupaten">
                                          <option value="">pilih kota/kabupaten</option>
                                          <option></option>
                                        </select>
                                    </div> 
                                </div>
								<div class="form-group">
								<label for="name">Alamat Pelanggan*</label>
								<input class="form-control" type="text" name="xalamatsekolah" placeholder="Alamat Pelanggan" />
                            </div>
                            
                            <!-- <div class="form-group">
								<label for="name">Kabupaten*</label>
								<div class="form-group">
                                        <select class="form-control" id="sel1" name="xidkabupaten">
                                        <?php 
                                            foreach ($kabupaten as $data):
                                        ?>
                                            <option value="<?php echo $data->id_kabupaten;?>"><?php echo $data->nama_kabupaten?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
							</div> -->

							<div class="form-group">
								<label for="name">No Handphone*</label>
								<input class="form-control" type="number" name="xnomer" placeholder="Nomer Handphone" />
                            </div>

							<div class="form-group">
								<label for="name">Password*</label>
								<input class="form-control" type="password" name="xpassword" placeholder="Password" />
                            </div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />                        
                        </form>

					</div>

					<div class="card-footer small text-muted">
						* Isi Semua Data
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
