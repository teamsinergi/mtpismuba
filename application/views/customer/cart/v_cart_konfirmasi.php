<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Penjualan Buku">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart_responsive.css'); ?> ">
</head>

<body>

<!-- Menu -->

<div class="menu">

	<!-- Search -->
	<div class="menu_search">
		<form action="#" id="menu_search_form" class="menu_search_form">
			<input type="text" class="search_input" placeholder="Search Item" required="required">
			<button class="menu_search_button"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
		</form>
	</div>

	<!-- Navigation -->
	<div class="menu_nav">
		<ul>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
		</ul>
	</div>
	<!-- Contact Info -->
	<div class="menu_contact">
		<div class="menu_phone d-flex flex-row align-items-center justify-content-start">
			<div><div><img src="assets2/images/phone.svg"></div></div>
			<div>+1 912-252-7350</div>
		</div>
		<div class="menu_social">
			<ul class="menu_social_list d-flex flex-row align-items-start justify-content-start">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="super_container">

	<!-- Header -->

	
	<?php $this->load->view("customer/partials/navbar.php") ?>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Keranjang Belanja <?php echo $this->session->userdata('nama_sekolah');?></div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="#">Home</a></li>
							<li>Your Cart</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">
							
							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Produk</li>
									<li>Kelas</li>
									<li>Harga</li>
									<li>Jumlah</li>
									<li>Total</li>
								</ul>
							</div>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">

									<!-- Cart Item -->
									<form action="<?php echo base_url().'customer/cart/simpan_konfirmasi_transaksi'?>" method="post" enctype="multipart/form-data" >	
									<?php $no=0; foreach($buku as $data):?>
										<?php foreach($tampung as $key):?>
											<?php if($data->id_buku == $key->id_buku): $no++?>
											
											<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
												<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
													<div><div class="product_number"><?php echo $no?></div></div>
													<div><div class="product_image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>" alt=""></div></div>
													<div class="product_name_container">
													<div class="product_id" hidden><a href="product.html"><input type="text" name="xbarang[]" value="<?php echo $data->id_buku?>"></a></div>
													<div class="product_name"><a href="product.html"><?php echo $data->nama_buku?> </a></div>
														<div class="product_text"><?php echo " Kelas ".$data->kelas_buku?></div>
													</div>
												</div>
												<div class="product_color product_text"><span>Color: </span><?php echo " Kelas ".$data->kelas_buku?></div>
												<div class="product_price product_text"><span>Harga: </span><?php echo number_format($data->harga_buku,0,',','.')?></div>
												<div class="product_quantity_container">
												<td width="50">
													<div class="product_price" hidden><a href="product.html"><input type="text" name="xjumlah[]" value="<?php echo $key->jumlah_buku?>"></a></div>
													<div class="product_price" hidden><a href="product.html"><input type="text" name="xidpembeli" value="<?php echo $key->id_pembeli?>"></a></div>
													<div class="product_price product_text"><?php echo number_format($key->jumlah_buku,0,',','.')?></div>
												</td>
												</div>
												<div class="product_price" hidden><a href="product.html"><input type="text" name="xtotal[]" value="<?php echo $key->total_harga?>"></a></div>
												<div class="product_total product_text"><span>Total: </span><?php echo number_format($key->total_harga,0,',','.')?></div>												
											</li>											
											<?php endif?>
										<?php endforeach?>
									<?php endforeach?>

                                    <?php foreach($carttotal as $data):?>                    
                                    <div class="row cart_extra_row">
                                        <div class="col-lg-6 cart_extra_col" style="width: 100%; max-width: 100%; flex: 0 0 100%;">
                                            <div class="cart_extra cart_extra_2" style="width: 100%">
                                                <div class="cart_extra_content cart_extra_total" style="width: 100%">
                                                    <div class="cart_extra_title">Total Belanja</div>
                                                    <ul class="cart_extra_total_list">
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Jenis Buku</div>
                                                            <div class="cart_extra_total_value ml-auto"><?php echo $data->banyak_buku-1?></div>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Total Buku</div>
                                                            <div class="cart_extra_total_value ml-auto"><?php echo number_format($data->jumlah_buku,0,',','.')?></div>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Sub Total Harga</div>
                                                            <div class="cart_extra_total_value ml-auto"><?php echo number_format($data->total_harga,0,',','.')?></div>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Ongkos Kirim</div>
                                                            <div class="cart_extra_total_value ml-auto">-</div>
                                                        </li>
                                                        <li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Total</div>
                                                            <div class="cart_extra_total_value ml-auto"><?php echo number_format($data->total_harga,0,',','.')?></div>
                                                        </li>
														<li class="d-flex flex-row align-items-center justify-content-start">
                                                            <div class="cart_extra_total_title">Silahkan melakukan pembayaran ke rekening BRI 0983763367282 atas nama IMRAN sebesar Rp. <?php echo $data->total_harga?></div>
                                                        </li>
                                                    </ul>
                                                    <!-- <div class="checkout_button trans_200"><a href="checkout.html">konfirmasi</a></div> -->
                                                </div>
												<br>
												<div class="row">
													<div class="col-2">
														<div class="form-group">
															<span>Alamat Tujuan</span>
														</div>
													</div>
													<div class="col-10">
														<div class="form-group">
															<input type="text" class="form-control" name="xtujuan" value="<?php echo $alamat?>">
														</div>
													</div>												
												</div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach?>
									<input class="btn btn-success col-xl-12" type="submit" name="btn" value="KONFIRMASI PEMBELIAN" /> 
                                    <hr>
                                    <div class="button button_clear trans_200">
                                    	<a href="<?php echo site_url('customer/cart')?>" role="button">Kembali</a></div>
                        			</form>
								</ul>
							</div>

							<!-- Cart Buttons -->
						</div>
					</div>
				</div>


			</div>
		</div>
<!-- Footer -->

		
	<?php $this->load->view("customer/partials/footer.php") ?>
	
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>
</body>
</html>