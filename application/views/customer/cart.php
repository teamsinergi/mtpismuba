<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Penjualan Buku">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart.css'); ?> " >

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/cart_responsive.css'); ?> " >
</head>
<body>

<!-- Menu -->

<div class="menu">

	<!-- Search -->
	<div class="menu_search">
		<form action="#" id="menu_search_form" class="menu_search_form">
			<input type="text" class="search_input" placeholder="Search Item" required="required">
			<button class="menu_search_button"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
		</form>
	</div>

	<!-- Navigation -->
	<div class="menu_nav">
		<ul>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
		</ul>
	</div>
	<!-- Contact Info -->
	<div class="menu_contact">
		<div class="menu_phone d-flex flex-row align-items-center justify-content-start">
			<div><div><img src="assets2/images/phone.svg" alt="https://www.flaticon.com/authors/freepik"></div></div>
			<div>+1 912-252-7350</div>
		</div>
		<div class="menu_social">
			<ul class="menu_social_list d-flex flex-row align-items-start justify-content-start">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<div class="header_overlay"></div>
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo">
				<a href="#">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div><img src="assets2/images/logo_1.png" alt=""></div>
						<div>Toko Buku ISMUBA</div>
					</div>
				</a>	
			</div>
			<div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav class="main_nav">
				<ul class="d-flex flex-row align-items-start justify-content-start">
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
					<li><a href="#">Lorem Ipsum</a></li>
				</ul>
			</nav>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
				<!-- Search -->
				<div class="header_search">
					<form action="#" id="header_search_form">
						<input type="text" class="search_input" placeholder="Search Item" required="required">
						<button class="header_search_button"><img src="assets2/images/search.png" alt=""></button>
					</form>
				</div>
				<!-- User -->
				<div class="user"><a href="#"><div><img src="assets2/images/user.svg" ><div>1</div></div></a></div>
				<!-- Cart -->
				<div class="cart"><a href="cart.html"><div><img class="svg" src="assets2/images/cart.svg" ></div></a></div>
				
			</div>
		</div>
	</header>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<div class="home">
			<div class="home_container d-flex flex-column align-items-center justify-content-end">
				<div class="home_content text-center">
					<div class="home_title">Shopping Cart</div>
					<div class="breadcrumbs d-flex flex-column align-items-center justify-content-center">
						<ul class="d-flex flex-row align-items-start justify-content-start text-center">
							<li><a href="#">Home</a></li>
							<li>Your Cart</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- Cart -->

		<div class="cart_section">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="cart_container">
							
							<!-- Cart Bar -->
							<div class="cart_bar">
								<ul class="cart_bar_list item_list d-flex flex-row align-items-center justify-content-end">
									<li class="mr-auto">Product</li>
									<li>Kelas</li>
									<li>Harga</li>
									<li>Jumlah</li>
									<li>Total</li>
								</ul>
							</div>

							<!-- Cart Items -->
							<div class="cart_items">
								<ul class="cart_items_list">

									<!-- Cart Item -->
									<li class="cart_item item_list d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-end justify-content-start">
										<div class="product d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start mr-auto">
											<div><div class="product_number">1</div></div>
											<div><div class="product_image"><img src="assets2/images/picture 1.jpg" alt=""></div></div>
											<div class="product_name_container">
												<div class="product_name"><a href="product.html">Buku Gambar Sugeng</a></div>
												<div class="product_text">Ini opsional aja sih</div>
											</div>
										</div>
										<div class="product_color product_text"><span>Color: </span>beige</div>
										<div class="product_price product_text"><span>Harga: </span>Rp.200.000</div>
										<div class="product_quantity_container">
											<div class="product_quantity ml-lg-auto mr-lg-auto text-center">
												<span class="product_text product_num">1</span>
												<div class="qty_sub qty_button trans_200 text-center"><span>-</span></div>
												<div class="qty_add qty_button trans_200 text-center"><span>+</span></div>
											</div>
										</div>
										<div class="product_total product_text"><span>Total: </span>Rp.200.000</div>
									</li>
								</ul>
							</div>

							<!-- Cart Buttons -->
							<div class="cart_buttons d-flex flex-row align-items-start justify-content-start">
								<div class="cart_buttons_inner ml-sm-auto d-flex flex-row align-items-start justify-content-start flex-wrap">
									<div class="button button_clear trans_200"><a href="categories.html">Reset</a></div>
									<div class="button button_continue trans_200"><a href="categories.html">Lanjutkan Belanja</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row cart_extra_row">
					<div class="col-lg-6">
						<div class="cart_extra cart_extra_1">
							<div class="cart_extra_content cart_extra_coupon">
								<div class="cart_extra_title">Coupon code</div>
								<div class="coupon_form_container">
									<form action="#" id="coupon_form" class="coupon_form">
										<input type="text" class="coupon_input" required="required">
										<button class="coupon_button">apply</button>
									</form>
								</div>
								<div class="coupon_text">Phasellus sit amet nunc eros. Sed nec congue tellus. Aenean nulla nisl, volutpat blandit lorem ut.</div>
								<div class="shipping">
									<div class="cart_extra_title">Shipping Method</div>
									<ul>
										<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
											<label class="radio_container">
												<input type="radio" id="radio_1" name="shipping_radio" class="shipping_radio">
												<span class="radio_mark"></span>
												<span class="radio_text">Next day delivery</span>
											</label>
											<div class="shipping_price ml-auto">$4.99</div>
										</li>
										<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
											<label class="radio_container">
												<input type="radio" id="radio_2" name="shipping_radio" class="shipping_radio">
												<span class="radio_mark"></span>
												<span class="radio_text">Standard delivery</span>
											</label>
											<div class="shipping_price ml-auto">$1.99</div>
										</li>
										<li class="shipping_option d-flex flex-row align-items-center justify-content-start">
											<label class="radio_container">
												<input type="radio" id="radio_3" name="shipping_radio" class="shipping_radio" checked>
												<span class="radio_mark"></span>
												<span class="radio_text">Personal Pickup</span>
											</label>
											<div class="shipping_price ml-auto">Free</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div> 
					<div class="col-lg-6 cart_extra_col" style="width: 100%; max-width: 100%; flex: 0 0 100%;">
						<div class="cart_extra cart_extra_2" style="width: 100%">
							<div class="cart_extra_content cart_extra_total" style="width: 100%">
								<div class="cart_extra_title">Cart Total</div>
								<ul class="cart_extra_total_list">
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Subtotal</div>
										<div class="cart_extra_total_value ml-auto">Hitung Sendiri</div>
									</li>
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Ongkos Kirim</div>
										<div class="cart_extra_total_value ml-auto">Bayar sendiri</div>
									</li>
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div class="cart_extra_total_title">Total</div>
										<div class="cart_extra_total_value ml-auto">Sekian ribu</div>
									</li>
								</ul>
								<div class="checkout_button trans_200"><a href="checkout.html">konfirmasi</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- Footer -->

		<footer class="footer">
			<div class="footer_content">
				<div class="container">
					<div class="row">
						
						<!-- About -->
						<div class="col-lg-4 footer_col">
							<div class="footer_about">
								<div class="footer_logo">
									<a href="#">
										<div class="d-flex flex-row align-items-center justify-content-start">
											<div class="footer_logo_icon"><img src="assets2/images/logo_2.png" alt=""></div>
											<div>Toko Buku ISMUBA</div>
										</div>
									</a>		
								</div>
								<div class="footer_about_text">
									<p>Kami meenjual berbagai macam buku sekolah kalau mau order ya di sini aja ngapain ketoko buku yg lain ? aneh. Kenapa Kami ? Karena toko buku kami di-develop oleh programmer yang nawaitunya kuat serta bertekad memintari anak bangsa (btw front endnya ganteng). Jadi, AYO ORDER DI KAMI SAJA JANGAN YANG LAIN DASAR ANEH</p>
								</div>
							</div>
						</div>

						<!-- Footer Links -->
						<div class="col-lg-4 footer_col">
							<div class="footer_menu">
								<div class="footer_title">Bantuan</div>
								<ul class="footer_list">
									<li>
										<a href="#"><div>Customer Service</div></a>
									</li>
									<li>
										<a href="#"><div>Konidin</div></a>
									</li>
									<li>
										<a href="#"><div>Lorem Opname</div></a>
									</li>
									<li>
										<a href="#"><div>Lorem Ipsum</div></a>
									</li>
									<li>
										<a href="#"><div>Leren Mbok</div></a>
									</li>
								</ul>
							</div>
						</div>

						<div class="footer_social">
									<div class="footer_title">Temukan Kami</div>
									<ul class="footer_social_list d-flex flex-row align-items-start justify-content-start">
										<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									</ul>
								</div>

						<!-- Footer Contact -->
					</div>
				</div>
			</div>
			<div class="footer_bar">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="footer_bar_content d-flex flex-md-row flex-column align-items-center justify-content-start">
								<div class="copyright order-md-1 order-2">

Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="#" target="_blank">Toko Buku ISMUBA</a>
</div>
								<nav class="footer_nav ml-md-auto order-md-2 order-1">
									<ul class="d-flex flex-row align-items-center justify-content-start">
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>
</body>
</html>