<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script>
        $(document).ready(function(){
            $('#provinsi').change(function(){
                var provinsi_id = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: '<?php echo base_url('assets/kota.php')?>',
                    data: 'prov_id='+provinsi_id,
                    success: function(response){
                        $('#kota').html(response);
                    }
                });
            })
        });
    </script>


</head>
<body>

<!-- Menu -->


<div class="super_container">

	<!-- Header -->

	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>


	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->
	

		<div class="products">
			<div class="container">
				<div class="box-cont">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center" style="margin-top: 1.5em; 
	font-family: 'Overpass', sans-serif;">Sunting Profil</div>
	<hr style="border: 1px solid #757575; opacity: 0.5;">
					</div>
				</div>
				<div class="row page_nav_row">
					<div class="col">
						<div class="page_nav">
							
						</div>
					</div>
				</div>

				


					
	<div class="prof-center"> 

	<img src="<?php echo base_url('assets/images/user.png');?> "/>

	<div class="text-prof jumbo">
		<h1> <?php echo $nama_pelanggan?> </h1></div>  

	<div class="block-prof" style="
	margin: 0 auto; width: 60%; height: 100%; margin-bottom: 3em;">
<br>

<br>
	<?php foreach($profile as $data):?>
		<form action="<?php echo base_url().'customer/profile/update_profile'?>" method="post" enctype="multipart/form-data" >
								<!-- dari gema -->
								<!-- <div class="form-group">
								<label for="name">Provinsi*</label>
								<div class="form-group">
									<select class="form-control" id="provinsi" name="xidkabupaten">
                                        <?php 
                                            foreach ($provinsi as $key):
                                        ?>
                                            <option value="<?php echo $key->id;?>"><?php echo $key->name?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                                </div>
								<div class="form-group">
								<label for="name">Kabupaten*</label>
								<div class="form-group">
									<select class="form-control" id="kota" name="xidkabupaten">
                                          <option value="">pilih kota/kabupaten</option>
                                          <option></option>
                                        </select>
                                    </div> 
                                </div> -->
								<!-- habis dasri gema -->
			<div class="group">      
			<input type="text" style = "font-family: 'Overpass', sans-serif; font-size: 1.5em;" name="xnama" value="<?php echo $data->nama_sekolah?>" required> 
			<span class="highlight"></span>
			<span class="bar"></span>
			<label>Nama</label>
			</div>

			<div class="group">      
			<input type="text" style = "font-family: 'Roboto Condensed', sans-serif; font-size: 1.5em;" name="xno" value="<?php echo $data->no_hp?>" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label>No. Telepon</label>
			</div>

			<div class="group">      
			<input type="text" style = "font-family: 'Roboto Condensed', sans-serif; font-size: 1.5em;" name="xalamat" value="<?php echo $data->alamat_sekolah?>" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label>Alamat</label>
			</div>

		<div class="row" id="editProf">
			<div class="col-md-6">
				<div class="group">  
					<h4>Provinsi</h4>
					<select class="form-control" id="provinsi" name="xidkabupaten">
                                        <?php 
                                            foreach ($provinsi as $key):
                                        ?>
                                            <option value="<?php echo $key->id;?>"><?php echo $key->name?></option>
                                        <?php endforeach;?>
                                        </select>
                            
				</div>
			</div>

		<div class="col-md-6">
					<div class="group">  
					<h4>Kabupaten/Kota</h4>
					<select class="form-control" id="kota" name="xidkabupaten">
                                        <?php 
                                            foreach ($kabupaten as $hey):
                                        ?>
                                            <option value="<?php echo $hey->id_kabupaten;?>"><?php echo $hey->name?></option>
                                        <?php endforeach;?>
                                        </select>
                            
				</div>

			</div>
			</div>

			<div class="group">      
			<input type="text" style = "font-family: 'Roboto Condensed', sans-serif; font-size: 1.5em;" name="xemail" value="<?php echo $data->email_sekolah?>" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label>E-mail</label>
			</div>

			<div class="group">      
			<input type="password" style = "font-family: 'Roboto Condensed', sans-serif; font-size: 1.5em;" name="xpassword" value="<?php echo $data->password_sekolah?>" required>
			<span class="highlight"></span>
			<span class="bar"></span>
			<label>Password</label>
			</div>
			<button class="btn-prof" type="submit">Simpan & Lanjutkan</button>
		</form>
	<?php endforeach?>
					</div>
				</div>
			</div>

			</div>

		</div>

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>
<script type="text/javascript">
	createEditableSelect(document.forms[0].myText);
</script>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>