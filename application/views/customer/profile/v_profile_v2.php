<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">
<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!------ Include the above in your HEAD tag ---------->


</head>
<body>
<?php if ($this->session->userdata('email_sekolah')==null){
             $this->load->view("customer/partials/navbarlogin.php");
    }else{
        $this->load->view("customer/partials/navbar.php");
    }
    ?>
<!-- Menu -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<div class="super_container">

     <div class="super_container_inner">
   
<div class="container profile-form">

    <div class="row justify-content-md-center">

                <div class="col-md-10">
                    <h1>BIODATA PENGGUNA</h1>
                </div>
                <div class="col-md-5">
                <img class="rounded mx-auto d-block" src="<?php echo base_url('assets/images/bapak.jpg');?> "/>
                </div>
                </div>

    <div class="col ">
            <div class="row justify-content-md-center">
               
                <div class="col-md-auto">
                    <h3><?php echo $nama_sekolah;?></h3>
                    <p style="text-align: center; margin:0 0 3px;">
                        Jl. Bapak kau jual cincau
                    </p>
                    <?php foreach($profile as $data):?>
                    <p  style="font-size: 12px; text-align: center; margin: 0;">
                        <i class="fa fa-map-pin"></i> <?php echo $data->prov.", ".$data->kab?> 
                    </p>
                    </div>
            </div>
                    <br>

            <div class="row justify-content-md-center ">
                <div class="col-md-5">
                    <h4><i class="fa fa-phone"></i> Nomer Telepon </h4>
                    <p style="text-align: center; color: #111; font-weight: bold;">
                    <?php echo $data->no_hp?>
                </p>
                    
                </div>

                <div class="col-md-5">
                    <h4><i class="fa fa-envelope"></i> Alamat E-mail </h4>
                    <p style="text-align: center; color: #111; font-weight: bold;">
                    <?php echo $data->email_sekolah?>
                </p>
                    
                </div>

                </div>
                <?php endforeach?>
                <br>

            <div class="row justify-content-md-center" style="margin-bottom: 20px;">
                <div class="col-md-auto">
                    <a href="<?php echo site_url ('customer/profile/edit_profile')?>">
                     <button class="btn btn-primary center-block">
                        Sunting Profil
                    </button>
                    </a>
                </div>
            </div>
               
     </div>

</div>



        <!-- Footer -->

    <?php $this->load->view("customer/partials/footer.php") ?>
    </div>
        
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>