<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.carousel.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">


</head>
<body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="scroll-down.js"> </script>
<!-- Menu -->
<script type="text/javascript">


	$(document).ready(function(){
  $("a").on('click', function(event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    } 
  });
});</script>



<div class="super_container">

	<!-- Header -->
	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>

	<div class="super_container_inner">
		<div class="big-box">
		<div class="super_overlay"></div>

		<!-- Home -->

	<!-- 	<div class="home" style="background-image: url(assets/images/testbg.jpg);"> -->
			<!-- Home Slider -->

	
				<div class="background_image" style="padding-top: 5%;" >
					<!-- <div class="container" style="width: 100%;"> -->
				<div class="row">
					
					<div id="carousel-example" class="carousel slide" data-ride="carousel" style="margin: 0 auto;">
						<!-- Wrapper for slides -->
						

						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<img src="assets2/images/banner/banner 1 alt.jpg" >
								</div>
							</div>

							<div class="item">
								<div class="row">
									<img src="assets2/images/banner/banner 2 alt.jpg">
								</div>
							</div>
						</div>

					</div>

				</div>
			<!-- </div> -->		
				</div>

		<!-- Products -->
			<!-- coresoul -->
			<div class="row"></div>

			<div class="rowaw">
				<div class="col" >
					<div class="col col-lg-6 offset-lg-3">
						<div class="section_title text-center"> Produk Terlaris
							<hr style="border-top: .9px solid grey"></div>
					</div>
				</div>
			</div>
				


				<div class="rowaw">
					<div class="colaw3" style="width: 1104px; margin: 0 auto;">
					
				<?php foreach($buku as $data):?>

					<a href="<?php echo site_url('customer/detail/lihat_barang/'.$data->id_buku)?>" style = "text-decoration: none;">
					<div class="columnx">
						
						<div class="productx">
							<div class="productx-image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>"/></div>

							<div class="productx-detail">
								<div class="">
									<div>
										<div>
											<div class="productx-name"> <?php echo $data->nama_buku?> </div>

											<div class="productx-cns" style="color: #111"> Kelas : <?php echo $data->kelas_buku?> | 
										Stok : <?php echo number_format($data->stok,0,',','.')?></div>
										</div>

										<center>

										<div class="productx-price"><span>Rp. </span><span></span><?php echo number_format($data->harga_buku,0,',','.')?></div></center>

									</div>
								</div>
							</div>
						</a>
						</div>
					</div>

							

				<?php endforeach?>


			</div>

				</div>

			</div>
		</div>
		</div>


	<!-- PRODUK SD -->
	<div class="products" style="background-color: #f5f5f5">
			<div class="containerx">
				<div class="row">
					<div class="col col-lg-6 offset-lg-3">
						<div class="section_title text-center"> Produk SD/MI
							<hr style="border-top: .9px solid grey"></div>
					</div>

<div class="product-center">
					
				
				<div class="row products_row">

					
					<!-- Product -->
					
				<?php foreach($buku6 as $data):?>

					<a href="<?php echo site_url('customer/detail/lihat_barang/'.$data->id_buku)?>" style = "text-decoration: none;">
					<div class="columnx">
						
						<div class="productx">
							<div class="productx-image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>"/></div>

							<div class="productx-detail">
								<div class="">
									<div>
										<div>
											<div class="productx-name"> <?php echo $data->nama_buku?> </div>

											<div class="productx-cns" style="color: #111"> Kelas : <?php echo $data->kelas_buku?> | 
										Stok : <?php echo $data->stok?></div>
										</div>

										<center>

										<center>

										<div class="productx-price"><span>Rp. </span><span></span><?php echo number_format($data->harga_buku,0,',','.')?></div></center>

									</div>
								</div>
								
							
								
							</div>
							
						</div>

					</div>
					</a>
				<?php endforeach?>
				</div>
				</div>

</div>
<br><br><br><br><br>
	<div><a href="<?php echo site_url('customer/main/buku_sd')?>"><button type="button" class="btn btn-info center" style="margin-top: 10%; font-size: 20px">Lainnya</button></a></div>			
</div>

</div>


	<!--PRODUK SMP-->
	<div class="products" >
			<div class="containerx">
				<div class="row">
					<div class="col col-lg-6 offset-lg-3">
						<div class="section_title text-center"> Produk SMP/MTs
							<hr style="border-top: .9px solid grey"></div>
					</div>

<div class="product-center">
					
				
				<div class="row products_row">

					
					<!-- Product -->
					
				<?php foreach($buku7 as $data):?>

					<a href="<?php echo site_url('customer/detail/lihat_barang/'.$data->id_buku)?>" style = "text-decoration: none;">
					<div class="columnx">
						
						<div class="productx">
							<div class="productx-image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>"/></div>

							<div class="productx-detail">
								<div class="">
									<div>
										<div>
											<div class="productx-name"> <?php echo $data->nama_buku?> </div>

											<div class="productx-cns" style="color: #111"> Kelas : <?php echo $data->kelas_buku?> | 
										Stok : <?php echo $data->stok?></div>
										</div>

										<center>

										<center>

										<div class="productx-price"><span>Rp. </span><span></span><?php echo number_format($data->harga_buku,0,',','.')?></div></center>

									</div>
								</div>
								
							
								
							</div>
							
						</div>

					</div>
					</a>
				<?php endforeach?>

				</div>
				</div>
</div>
<br><br><br><br><br>
	<div><a href="<?php echo site_url('customer/main/buku_smp')?>"><button type="button" class="btn btn-info center" style="margin-top: 2%; font-size: 20px">Lainnya</button></a></div>
</div>
			
</div>

	<!--PRODUK SMA -->
	<div class="products" style="background-color: #f5f5f5">
			<div class="containerx">
				<div class="row">
					<div class="col col-lg-6 offset-lg-3">
						<div class="section_title text-center"> Produk SMA/MA
							<hr style="border-top: .9px solid grey"></div>
					</div>

<div class="product-center">
					
				
				<div class="row products_row">

					
					<!-- Product -->
					
				<?php foreach($buku8 as $data):?>

					<a href="<?php echo site_url('customer/detail/lihat_barang/'.$data->id_buku)?>" style = "text-decoration: none;">
					<div class="columnx">
						
						<div class="productx">
							<div class="productx-image"><img src="<?php echo base_url('assets/images/'.$data->foto_barang) ?>"/></div>

							<div class="productx-detail">
								<div class="">
									<div>
										<div>
											<div class="productx-name"> <?php echo $data->nama_buku?> </div>

											<div class="productx-cns" style="color: #111"> Kelas : <?php echo $data->kelas_buku?> | 
										Stok : <?php echo $data->stok?></div>
										</div>

										<center>

										<center>

										<div class="productx-price"><span>Rp. </span><span></span><?php echo number_format($data->harga_buku,0,',','.')?></div></center>

									</div>
								</div>
								
							
								
							</div>
							
						</div>

					</div>
					</a>
				<?php endforeach?>

				</div>
				</div>
</div>
<br><br><br><br><br>
<div><a href="<?php echo site_url('customer/main/buku_sma')?>"><button type="button" class="btn btn-info center" style="margin-top: 2%; font-size: 20px">Lainnya</button></a></div>
</div>
				
</div>


	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>
