<!DOCTYPE html>
<html lang="en">
<head>
  <title>Masuk</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('aslog/images/icons/favicon.ico')?>"/>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/bootstrap/css/bootstrap.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/animate/animate.css')?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/css-hamburgers/hamburgers.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/select2/select2.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/util.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/main.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/custom_style.css')?>">
  <link rel="icon" href="<?php echo base_url('assets/images/Icon_ismuba.ico'); ?>">

</head>
<body>
  
  <div class="limiter">
    <div class="container-login100">
      
      <div class="wrap-login100 wrap-icon-ismuba">
        <img class="icon-form-login" src="<?php echo base_url('aslog/images/DIKDASMEN1.png')?>">
        <br><br>
        <center>
          <div>
            <b style="color: #f6ff00">MAJLIS DISDAKMEN PUSAT MUHAMMADIYAH</b>
          </div>
        </center>
          
      </div>
      
      <center>
        <div class="line-login"></div>
      </center>

      
      <div class="wrap-login100 p-t-190 p-b-30 wrap-form-login">
        <form class="login100-form validate-form" role="form" method="post" action="<?php echo base_url('customer/login/login_user_customer'); ?>">
          
          <span class="login100-form-title p-t-20 p-b-45">
            Masuk
          </span>

          <?php
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');
              $emailsukses = $this->session->flashdata('emailterkirim'); 
              $emaileror = $this->session->flashdata('emaileror');
              if($emailsukses){?>
                <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $emailsukses?></strong>
                </div>
                <?php }elseif($emaileror){?>
                <div class="alert alert-danger alert-dismissible" style="font-size:10px;">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $emaileror?></strong>
                </div>
                <?php }?>
                <?php
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $error_msg?></strong>
                    </div>
                    <?php
                  }
                  ?>
                  <?php 
                    $aktifasiberhasil = $this->session->flashdata('pesanaktifasi');
                    if($aktifasiberhasil){ ?>
                    <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $aktifasiberhasil?></strong>
                    </div>
                  <?php }?>
                  <?php 
                    $linkpassword = $this->session->flashdata('linkpassword');
                    if($linkpassword){ ?>
                    <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $linkpassword?></strong>
                    </div>
                  <?php }?>
                  <?php 
                    $gantipass = $this->session->flashdata('newpassword');
                    if($gantipass){ ?>
                    <div class="alert alert-info alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $gantipass?></strong>
                    </div>
                  <?php }?>
                  <label><font color="white">Email</font></label>
                  <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
                  
                    <input class="input100" placeholder="@" name="user_email" type="email" autofocus required>
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                      <i class="fa fa-user"></i>
                    </span>
                  </div>
                  <label><font color="white">Password</font></label>
                  <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
                    <input class="input100" placeholder="*******" name="user_password" type="password" required>
                    <span class="focus-input100"></span>
                    <span class="symbol-input100">
                      <i class="fa fa-lock"></i>
                    </span>
                  </div>

                  <div class="container-login100-form-btn p-t-10">
                    <button class="login100-form-btn">
                      Masuk
                    </button>
                  </div>

                  <div class="text-center w-full p-t-25 p-b-230">
                  <b><font color="wihite">Belum Punya Akun ?</font></b>
                    <a href="<?php echo base_url('customer/login'); ?>" class="txt1">
                      <b>Daftar</b>
                    </a><br>
                    <a style="color: white;" href="<?php echo site_url('customer/login/lupa_password')?>"><b>Lupa Password.?</b></a>
                  </div>
                  <div class="d-flex justify-content-center">
    
                  </div>
        </form>
      </div>
    </div>
  
  

  
  
  <script src="<?php echo base_url('aslog/vendor/jquery/jquery-3.2.1.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/popper.js')?>"></script>
  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/bootstrap.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/select2/select2.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/js/main.js')?>"></script>

</body>
</html>

