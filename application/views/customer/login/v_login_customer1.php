 
<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php $this->load->view("customer/login/csslog.php") ?>
  <title>Login</title>
</head>
<body id="page-top">
<div class="bg-image"></div>
<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3 style="color: black;"><b style="color: white">Masuk</b></h3>
  </center>
   </div>
               <?php
              $success_msg= $this->session->flashdata('success_msg');
              $error_msg= $this->session->flashdata('error_msg');
              $emailsukses = $this->session->flashdata('emailterkirim'); 
              $emaileror = $this->session->flashdata('emaileror');
              if($emailsukses){?>
                <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $emailsukses?></strong>
                </div>
                <?php }elseif($emaileror){?>
                <div class="alert alert-danger alert-dismissible" style="font-size:10px;">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong><?php echo $emaileror?></strong>
                </div>
                <?php }?>
                <?php
                  if($error_msg){
                    ?>
                    <div class="alert alert-danger alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $error_msg?></strong>
                    </div>
                    <?php
                  }
                  ?>
                  <?php 
                    $aktifasiberhasil = $this->session->flashdata('pesanaktifasi');
                    if($aktifasiberhasil){ ?>
                    <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $aktifasiberhasil?></strong>
                    </div>
                  <?php }?>
                  <?php 
                    $linkpassword = $this->session->flashdata('linkpassword');
                    if($linkpassword){ ?>
                    <div class="alert alert-success alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $linkpassword?></strong>
                    </div>
                  <?php }?>
                  <?php 
                    $gantipass = $this->session->flashdata('newpassword');
                    if($gantipass){ ?>
                    <div class="alert alert-info alert-dismissible" style="font-size:10px;">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong><?php echo $gantipass?></strong>
                    </div>
                  <?php }?>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('customer/login/login_user_customer'); ?>">
     <div >
       <font color="red"> <i class="fas fa-user"></i></font>
       <span><font color="white"> Alamat Email </font></span>
      </div>
     <div class="input-group form-group">
     <input class="form-control" placeholder="@" name="user_email" type="email" autofocus required>     
     </div>
     <div >
       <font color="red"> <i class="fas fa-key"></i></font>
       <span><font color="white"> Kata Sandi </font></span>
      </div>
     <div class="input-group form-group">
     <input class="form-control" placeholder="*******" name="user_password" type="password" required>
     </div>
      <div class="row align-items-center remember">
      <input type="checkbox">Remember Me
     </div> 
     <div class="form-group">
      <input type="submit" value="Login" class="btn float-right login_btn">
     </div>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links" style="color: white;">
     Tidak Punya Akun?<a style="color: white;" href="<?php echo base_url('customer/login'); ?>"><b> Daftar </b></a>
    </div>
   <div class="d-flex justify-content-center">
    <a style="color: white;" href="<?php echo site_url('customer/login/lupa_password')?>"><b>Lupa Password.?</b></a>
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
