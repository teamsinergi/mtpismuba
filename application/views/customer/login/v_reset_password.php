<html>
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body>
    
<div class="container">
  <h2>Form Reset Password ISMUBA</h2>
  <p>Masukkan Email Akun Anda</p>
  <form action="<?php echo base_url('customer/login/reset_password'); ?>" method="POST">
    <div class="form-group">
      <label for="usr">Email</label>
      <input type="email" class="form-control" id="usr" name="xemail" placeholder="contoh : gemaantika@gmail.com" required>
    </div>
    <a href="<?php echo base_url('customer/login/login_view'); ?>" class="btn btn-danger" role="button">Kembali</a>
    <button type="submit" class="btn btn-primary">Kirim</button>
  </form>
  
</div>

	<?php $this->load->view("admin/_partials/js.php") ?>
</body>
</html>