<!-- Background Dongker -->
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Daftar</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="icon" type="image/png" href="<?php echo base_url('aslog/images/icons/favicon.ico')?>"/>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/bootstrap/css/bootstrap.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/font-awesome-4.7.0/css/font-awesome.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/animate/animate.css')?>">
  
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/css-hamburgers/hamburgers.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/vendor/select2/select2.min.css')?>">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/util.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('aslog/css/main.css')?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/custom_style.css')?>">
  <link rel="icon" href="<?php echo base_url('assets/images/Icon_ismuba.ico'); ?>">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function(){
                $('#provinsi').change(function(){
                    var provinsi_id = $(this).val();
                    $.ajax({
                        type: 'POST',
                        url: '<?php echo base_url('assets/kota.php')?>',
                        data: 'prov_id='+provinsi_id,
                        success: function(response){
                            $('#kota').html(response);
                        }
                    });
                })
            });
        </script>

</head>
<body>
  
  <div class="limiter">
    <div class="container-login100 persent92">
     
      <div class="wrap-login100 wrap-icon-ismuba adjust-left-register">
        <img class="icon-form-login" src="<?php echo base_url('aslog/images/DIKDASMEN1.png')?>">
        <br><br>
        <center>
          <div>
            <b style="color: #f6ff00">MAJLIS DISDAKMEN PUSAT MUHAMMADIYAH</b>
          </div>
        </center>
          
      </div>
     
        
        <!-- <div style=" height: 470px; border: 1px white solid; margin-top: -18% "> -->
        <div class="line-login">
        </div>
        

      
      <div class="wrap-login100 wrap-form-login">
        <form class="login100-form validate-form wrap-login-register" role="form" method="post" action="<?php echo base_url('customer/login/register_customer'); ?>">
          <span class="login100-form-title p-t-20 p-b-45" style="margin-bottom: -2%">
            Daftar
          </span>
          <div class="name" style="color: #fff">
            <?php
              $error_msg=$this->session->flashdata('error_msg');
              if($error_msg){
                echo $error_msg;
              }
            ?>

          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="name" style="color: #fff;">Nama Sekolah*</label>
                <input class="input-register" placeholder="Nama Sekolah" name="xnama" type="text" autofocus>
              </div>
                    
              <div class="form-group">
                  <label for="name" style="color: #fff;">Level*</label>
                  <div class="form-group">
                      <select class="input-register" id="sel1" name="xidlevel" required>
                      <?php 
                          foreach ($level as $data):
                      ?>
                          <option value="<?php echo $data->id_level;?>"><?php echo $data->nama_level?></option>
                      <?php endforeach;?>
                      </select>
                  </div> 
              </div>

              <div class="form-group">
                  <label for="name" style="color: #fff;">Provinsi*</label>
                  <div class="form-group">
                      <select class="input-register" id="provinsi" name="xidkabupaten" required>
                      <?php 
                          foreach ($provinsi as $data):
                      ?>
                          <option value="<?php echo $data->id;?>"><?php echo $data->name?></option>
                      <?php endforeach;?>
                      </select>
                  </div> 
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                  <label for="name" style="color: #fff;">Kabupaten*</label>
                  <div class="form-group">
                      <select class="input-register" id="kota" name="xidkabupaten" required>
                        <option value="">pilih kota</option>
                        <option></option>
                      </select>
                  </div> 
              </div>

              <!-- <div class="form-group">
                  <input class="input100" placeholder="Alamat" name="xalamat" type="text" autofocus>
              </div> -->

              <div class="form-group">
              <label for="name" style="color: #fff;">Email*</label>
                  <input class="input-register" placeholder="E-mail" name="xemail" type="email" autofocus>
              </div>

              <!-- <div class="form-group">
                  <input  class="input100" placeholder="Nomer Telepon" name="xnomerhp" type="number" autofocus>
              </div> -->

              <div class="form-group">
              <label for="name" style="color: #fff;">Password*</label>
                  <input class="input-register" placeholder="Kata Sandi" name="xpassword" type="password" value="">
              </div>
            </div>
          </div>

          <div class="container-login100-form-btn p-t-10">
            <button class="login100-form-btn">
              Daftar
            </button>
          </div>

          <div class="text-center w-full p-t-25 p-b-230">
          <b><font color="wihite"> Sudah Punya Akun ?</font></b>
            <a href="<?php echo base_url('customer/login/login_view'); ?>" class="txt1">
              <b>Masuk</b>
            </a>
          </div>

          
        </form>
      </div>

    </div>
  </div>
  
  

  
  
  <script src="<?php echo base_url('aslog/vendor/jquery/jquery-3.2.1.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/popper.js')?>"></script>
  <script src="<?php echo base_url('aslog/vendor/bootstrap/js/bootstrap.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/vendor/select2/select2.min.js')?>"></script>

  <script src="<?php echo base_url('aslog/js/main.js')?>"></script>

</body>
</html>
<!-- End Background Dongker -->





