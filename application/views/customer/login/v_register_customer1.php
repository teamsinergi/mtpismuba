<!DOCTYPE html>
<html lang="en">
<head>
  
  <?php  $this->load->view("customer/login/csslogres.php") ?>
  <title>Register Akun</title>
</head>
<body id="page-top">

<div class="container">
 <div class="d-flex justify-content-center h-100">
  <div class="card">
   <div class="card-header">
   <center>
    <h3><b style="color: white">Daftar</b></h3>
  </center>
   </div>
   
   <div class="name" style="color: #fff">
   <?php
                $error_msg=$this->session->flashdata('error_msg');
                  if($error_msg){
                    echo $error_msg;
                    }
   ?>

                   </div>

   <div class="card-body">
    <form role="form" method="post" action="<?php echo base_url('customer/login/register_customer'); ?>">
     <fieldset>
                <div class="form-group">
                    <input class="form-control" placeholder="Nama Sekolah" name="xnama" type="text" autofocus>
                </div>
                
                                <div class="form-group">
                                    <label for="name" style="color: #fff;">Level*</label>
                                    <div class="form-group">
                                        <select class="form-control" id="sel1" name="xidlevel" required>
                                        <?php 
                                            foreach ($level as $data):
                                        ?>
                                            <option value="<?php echo $data->id_level;?>"><?php echo $data->nama_level?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                                </div>

                              <div class="form-group">
                                    <label for="name" style="color: #fff;">Kabupaten*</label>
                                    <div class="form-group">
                                        <select class="form-control" id="sel1" name="xidkabupaten" required>
                                        <?php 
                                            foreach ($kabupaten as $data):
                                        ?>
                                            <option value="<?php echo $data->id_kabupaten;?>"><?php echo $data->nama_kabupaten?></option>
                                        <?php endforeach;?>
                                        </select>
                                    </div> 
                                </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="Alamat" name="xalamat" type="text" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="E-mail" name="xemail" type="email" autofocus>
                              </div>

                              <div class="form-group">
                                  <input  class="form-control" placeholder="Nomer Telepon" name="xnomerhp" type="number" autofocus>
                              </div>

                              <div class="form-group">
                                  <input class="form-control" placeholder="Kata Sandi" name="xpassword" type="password" value="">
                              </div>


                             <div class="form-group">
                            <input type="submit" value="Daftar" class="btn float-right login_btn">
     </div>

                          </fieldset>
    </form>
   </div>
   <div class="card-footer">
    <div class="d-flex justify-content-center links">
    <center><b>Sudah Punya Akun ?</b> <br></b><a style="color: white;" href="<?php echo base_url('customer/login/login_view'); ?>"><b> Masuk </b></a></center>
    </div>
   </div>
  </div>
 </div>
</div>


<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>