<html>
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body>
    
<div class="container">
  <h2>Form Reset Password ISMUBA</h2>
  <p>Silahkan masukkan password baru anda</p>
  <form action="<?php echo base_url('customer/login/password_baru'); ?>" method="POST">
    <div class="form-group" hidden>
      <label for="usr">id sekolah</label>
      <input type="number" class="form-control" id="usr" name="xid" value="<?php echo $id_sekolah?>" required>
    </div>
    <div class="form-group">
      <label for="usr">Email</label>
      <input type="email" class="form-control" id="usr" name="xemail" value="<?php echo $email_sekolah?>" readonly>
    </div>
    <div class="form-group">
      <label for="usr">Password</label>
      <input type="text" class="form-control" id="usr" name="xpassword" required>
    </div>
    <a href="<?php echo base_url('customer/login/login_view'); ?>" class="btn btn-danger" role="button">Kembali</a>
    <button type="submit" class="btn btn-primary">Kirim</button>
  </form>
  
</div>

	<?php $this->load->view("admin/_partials/js.php") ?>
</body>
</html>