$('.flipBtn').on('click', function() {
  var itemNum = $('.pane.one .active');  // get an active item in a stack
  var crnt = $('.pane.one .card').index( itemNum );  // find out the index of an active card
  var end = $('.pane.one .card').length-1;  // find out how many cards are in a stack
  
  ($(this).val() == 'Next') ? crnt++ : crnt--; // next / previous
  
  if (crnt > end) {  // at the end, so start over
    crnt = 0;
  } else if (crnt < 0) {  // at the front, so go to the end
    crnt = end;
  }
  
  $('.pane .active').removeClass('active');
  $('.pane .card:nth-child('+ (crnt+1) +')').addClass('active');
})