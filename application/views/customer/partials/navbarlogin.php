
<!DOCTYPE html>
<html>
<head>
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
<body >

	<style type="text/css">
	.dropbtnnv {
		background: transparent;
  color: #2e2e2e;
  font-size: 20px;
  border: none;
}

.dropdownnv {
  position: relative;
  display: inline-block;
}

.dropdown-contentnv {
  display: none;
  position: absolute;
  background-color: #fff;
  min-width: 170px;
  border-bottom-right-radius: 6px;
  border-bottom-left-radius: 6px;
  z-index: 1;
}

.dropdown-contentnv a {
  color: black;
  padding: 12px 16px;
  border-bottom-right-radius: 6px;
  border-bottom-left-radius: 6px;
  text-decoration: none;
  display: block;
}

.dropdown-contentnv a:hover {background-color: #ddd;}

.dropdownnv:hover .dropdown-contentnv {display: block;}

.dropdownnv:hover .dropbtnnv {color: #2fce98;}

</style>
<!-- 	
<div class="menu"> -->

	<!-- Search -->
	<!-- <div class="menu_search">
		<form action="#" id="menu_search_form" class="menu_search_form">
			<input type="text" class="search_input" placeholder="Search Item" required="required">
			<button class="menu_search_button"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
		</form>
	</div> -->

	<!-- Navigation -->
	<!-- <div class="menu_nav">
		<ul>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
			<li><a href="#">Lorem Ipsum</a></li>
		</ul>
	</div> -->
	<!-- Contact Info -->
	<!-- <div class="menu_contact">
		<div class="menu_phone d-flex flex-row align-items-center justify-content-start">
			<div><div><img src="assets2/images/phone.svg" alt="https://www.flaticon.com/authors/freepik"></div></div>
			<div>+1 912-252-7350</div>
		</div>
		<div class="menu_social">
			<ul class="menu_social_list d-flex flex-row align-items-start justify-content-start">
				<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div> --><script>
function openNav() {
  document.getElementById("mySidenav").style.width = " 350px";

}



function closeNav() {
  document.getElementById("mySidenav").style.width = "0";

  document.getElementById("main").style.marginLeft = "0";

  
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>



<header class="header">
	<div id="mySidenav" class="sidenav"> 
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

	
  <h1> MENU </h1>

  <a href="<?php echo site_url('customer/main/buku_sd')?>">SD/MI</a>
  <hr>
  <a href="<?php echo site_url('customer/main/buku_smp')?>">SMP/MTs</a>
  <hr>
  <a href="<?php echo site_url('customer/main/buku_sma')?>">SMA/MA</a>
  <hr>
  <a href="<?php echo site_url('customer/contact_us')?>">Kontak Kami</a>
  <hr>
  <a href="<?php echo site_url('customer/main/tentang_kami')?>">Tentang Kami</a>
  <hr>
<a href="<?php echo site_url('customer/faq')?>">Bantuan</a>




  <!-- <div class="menu-contact">
		<div class="menu_phone d-flex flex-row align-items-center justify-content-start">
			<div><div><img src="<?php echo base_url('assets2/images/phone.svg'); ?>"></div></div>
			<div>+1 912-252-7350</div>
		</div>

		<div class="header-search">
					<form action="<?php echo base_url().'customer/main/caribukuku'?>" method="post" enctype="multipart/form-data" >
						<input type="text" class="search-input" name="xcari" placeholder="Search Item" required="required">
						<button class="menu-search-button" type="submit"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
					</form>
				</div>



	</div>
 -->
</div>
		<div class="header_overlay" ></div>
		<div id="myModal" class="header_content d-flex flex-row align-items-center justify-content-start" >
			<div class="logo">
				<a href="<?php echo site_url('customer')?>">
					<div class="d-flex flex-row align-items-center justify-content-start">
						<div><img src="<?php echo site_url('assets/images/logo dik.png')?>" width="70px"></div>
						<div>Toko Buku ISMUBA</div>
					</div>
				</a>	
			</div>

			<div class="hamburger"> <i class="fa fa-bars" aria-hidden="true" onclick="openNav();" ></i></div>

			<nav class="main_nav">
				<ul class="d-flex flex-row align-items-start justify-content-start">
					<li><div class="dropdownnv">
  							<button class="dropbtnnv">Produk <i class="fa fa-caret-down"></i> </button>
  								<div class="dropdown-contentnv">
    								<a href="<?php echo site_url('customer/main/buku_sd')?>">SD/MI</a>
    								<hr style="margin-top: 0px;margin-bottom: 0px;">
    								<a href="<?php echo site_url('customer/main/buku_smp')?>">SMP/MTs</a>
    								<hr style="margin-top: 0px;margin-bottom: 0px;">
    								<a href="<?php echo site_url('customer/main/buku_sma')?>">SMA/MA/SMK</a>
  								</div>
						</div>
					</li>
					<li><a style="text-decoration: none;" href="<?php echo site_url('customer/contact_us')?>">Kontak Kami</a></li>
					<li><a style="text-decoration: none;" href="<?php echo site_url('customer/main/tentang_kami')?>">Tentang Kami</a></li>
					<li><a style="text-decoration: none;" href="<?php echo site_url('customer/faq')?>">Bantuan</a></li>
				</ul>
			</nav>
			<div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
				<!-- Search -->

				<div class="header_search">
					<form action="<?php echo base_url().'customer/main/caribukuku'?>" method="post" enctype="multipart/form-data" >
						<input type="text" class="search_input" name="xcari" placeholder="Cari Produk" required="required">
						<button class="menu_search_button" type="submit"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
					</form>
				</div>

				<!-- <div class="menu_search">
		<form action="#" id="menu_search_form" class="menu_search_form">
			<input type="text" class="search_input" placeholder="Search Item" required="required">
			<button class="menu_search_button"><img src="<?php echo base_url('assets2/images/search.png');?>" /></button>
		</form>
	</div> -->
				
  <div class="dropdown">
    <button class="btn" type="button" data-toggle="dropdown" style="margin-right: 10px; background-color: transparent;">

				<div class="user" style="margin: 0 auto" data-toggle="tooltip" data-placement="bottom"title="Pengguna"><a href="#"><div><img src="<?php echo site_url('assets2/images/user.svg')?>"></div></a></div> </button>
		<ul class="dropdown-menu">
      <li><a href="<?php echo site_url('customer/login/login_view')?>">Masuk</a></li>
    </ul>
</div>
				<!-- Cart -->
				<div class="cart" data-toggle="tooltip" data-placement="bottom"title="Keranjang"><a href="<?php echo site_url('customer/cart')?>"><div><img class="svg"  src="<?php echo site_url('assets2/images/cart.svg')?>"></div></a></div>
				
			</div>
		</div>
	</header>

</body>
</html>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hi <?php echo $this->session->userdata('nama_sekolah'); ?>, anda yakin ingin keluar ?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Tekan tombol log out jika iya, tekan tombol cancel untuk kembali berbelanja</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="<?php echo base_url('customer/login/user_logout_hiya');?>">Logout</a>
      </div>
    </div>
  </div>
</div>

