
<footer class="footer" style="border-top: 2px solid #2fce98">
			<div class="footer_content">
				<div class="container">
					<div class="row">
						
						<!-- About -->

						<div class="col-lg-8 footer_col">
							<div class="footer_about">
								<div class="footer_logo">
									<a href="#" style="text-decoration: none;">
										<div class="d-flex flex-row align-items-center justify-content-start">
											<div class="footer_logo_icon"><i class="fa fa-shopping-bag" style="color: #4a4a4a; font-size: 30px;"></i> </div>
											<div>Toko Buku ISMUBA</div>
										</div>
									</a>		
								</div>
								<div class="footer_about_text">
									<p>  Toko buku ISMUBA merupakan toko buku yang menyediakan buku - buku pelajaran sekolah yang ber-alamat di : 
										<br>Jl. Rejowinangun No. 3, Rejowinangun, Kotagede, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55171</p>
								</div>
							</div>
						</div>

						<!-- Footer Links -->
						<!-- <div class="col-lg-4 footer_col">
							<div class="footer_menu">
								<div class="footer_title">Bantuan</div>
								<ul class="footer_list">
									<li>
										<a href="#"><div>Customer Service</div></a>
									</li>
									<li>
										<a href="#"><div>Dummy Menu</div></a>
									</li>
									<li>
										<a href="#"><div>Dummy Menu</div></a>
									</li>
									<li>
										<a href="#"><div>Dummy Menu</div></a>
									</li>
								</ul>
							</div>
						</div> -->
<div class="col-lg-4 footer_col">
						<div class="footer_social">
									<div class="footer_title">Temukan Kami</div>
									<ul class="footer_social_list d-flex flex-row align-items-start justify-content-start">
										<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
										<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									</ul>
								</div>
								</div>

						<!-- Footer Contact -->
					</div>
				</div>
			</div>
			<div class="footer_bar">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="footer_bar_content d-flex flex-md-row flex-column align-items-center justify-content-start">
								<div class="copyright order-md-1 order-2">

Copyright &copy;<script>document.write(new Date().getFullYear());</script> <a href="#" target="_blank">DIKDASMEN</a>
</div>
								<!-- <nav class="footer_nav ml-md-auto order-md-2 order-1">
									<ul class="d-flex flex-row align-items-center justify-content-start">
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
										<li><a href="#">Nav</a></li>
									</ul>
								</nav> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
