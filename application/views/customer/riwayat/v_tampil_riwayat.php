<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.carousel.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">


</head>
<body >

<!-- Menu -->



<div class="super_container">

	<!-- Header -->
	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<!-- Products -->

		<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="section_title text-center">Riwayat Pembelian</div>
					</div>
					<?php 
                    $bukti = $this->session->flashdata('pesanbukti');
                    if($bukti){ ?>
					<div class="alert alert-success alert-dismissible col-12">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
							<center><strong><?php echo $bukti?></strong></center>
					</div>
					<?php }?>
				</div>
				<div class="row products_row">
					<!-- Product -->
					<table width="100%">
					<br>
						<tr>
							<td>	
									<?php foreach($proses as $data):?>
										<div class="container">
										<!-- Produk 1 -->
											<div class="row">
												<div class="col-xl-12">
													<div class="panel panel-default card">
														<div class="panel-body post-body">
															<div class="post-author">
																<p style="text-align:justify;">
																	<div class="alert alert-info">
																		<center><a href="#" class="alert-link">Transaksi tanggal : <?php echo $data->tanggal?></a></center>
																	</div>
																	<div class="alert alert-info">
																			<center><strong>Pesanan Anda Sedang Di Proses</strong></center>
																	</div>
																	<div class="container">
																	<div class="container">     
																	<div class="row">
																		<div class="col-sm-4">
																			<h5>Banyak Buku</h5>
																		</div>
																		<div class="col-sm-6">
																		<h5 align="right"><?php echo $data->banyak_barang?> Unit</h5>
																		</div>
																		</div>
																		<div class="row">
																		<div class="col-sm-4">
																			<h5>Total Harga</h5>
																		</div>
																		<div class="col-sm-6">
																		<h5 align="right">Rp. <?php echo number_format($data->total_harga,0,',','.')?></h5>
																		</div>
																		</div>
																	</div>
																	</div>
																<!-- <p style="text-align: right;"><a href="<?php echo site_url('customer/riwayat/rincian/'.$data->tanggal)?>"><button class="btn btn-default">Tampilkan Rincian Pesanan</button></a></p> -->
																<p style="text-align: right;"><a href="<?php echo site_url('customer/riwayat/rincian_pesanan/'.$data->kode_transaksi)?>"><button class="btn btn-default">Tampilkan Rincian Pesanan</button></a></p>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach?>

							</td>
						</tr>
					</table>
				</div>
				
				<div class="row products_row">
					<!-- Product -->
					<table width="100%">
					<br>
						<tr>
							<td>	
									<?php foreach($riwayat as $data):?>
										<div class="container">
										<!-- Produk 1 -->
											<div class="row">
												<div class="col-xl-12">
													<div class="panel panel-default card">
														<div class="panel-body post-body">
															<div class="post-author">
																<p style="text-align:justify;"><div class="alert alert-success">
																		<center><a href="#" class="alert-link">Transaksi tanggal : <?php echo $data->tanggal?></a></center>
																	</div>
																	<div class="container">
																	<div class="container">     
																	<div class="row">
																		<div class="col-sm-4">
																			<h5>Banyak Buku</h5>
																		</div>
																		<div class="col-sm-6">
																		<h5 align="right"><?php echo $data->banyak_barang?> Unit</h5>
																		</div>
																		</div>
																		<div class="row">
																		<div class="col-sm-4">
																			<h5>Total Harga</h5>
																		</div>
																		<div class="col-sm-6">
																		<h5 align="right">Rp. <?php echo $data->total_harga?></h5>
																		</div>
																		</div>
																	</div>
																	</div>
																	<div class="row">
																		<div class="col-8">
																			<?php if($data->status != 2){
																				echo "";} ?>
																		</div>
																		<div class="col-4">
																		<p style="text-align: right;"><a href="<?php echo site_url('customer/riwayat/rincian/'.$data->kode_transaksi)?>"><button class="btn btn-default">Tampilkan Rincian Pesanan</button></a></p>
																		</div>
																	</div>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach?>

							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>

		<!-- Boxes -->


		
		<!-- Footer -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->



	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>