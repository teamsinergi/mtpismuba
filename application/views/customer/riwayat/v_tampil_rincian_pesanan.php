<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.carousel.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/owl.theme.default.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/OwlCarousel2-2.2.1/animate.csss'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">


</head>
<body >

<!-- Menu -->



<div class="super_container">

	<!-- Header -->
	<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<!-- Products -->

		<div class="products">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<br>
						<br>
							<div class="section_title text-center" style="font-size:25px;">Kode Transaksi : <?php echo $kode?></div>
							<button type="button" class="btn btn-primary btn-block">Status Barang : Diproses</button> 
						<br>	
					</div>
				</div>
				<div class="container-fluid">
				<!-- DataTables -->
				<div class="card mb-3">
					<div class="card-header">
					Rincian Pembelian Buku Kemuhammadiyahan
					</div>
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Nomer*</th>
										<th>Nama Barang*</th>
                    <th>Kelas*</th>
                    <th>Harga*</th>
										<th>Banyak*</th>
										<th>Total Harga*</th>
									</tr>
								</thead>
								<tbody>
                    <?php
										$no = 0; 
                    foreach ($detailtransaksi as $data):
                    $no++; ?>
									<tr>
										<td>
											<?php echo $no?>
										</td>
										<td>
											<?php echo $data->nama_buku ?>
										</td>
                    <td>
                      	<?php echo "Kelas ".$data->kelas_buku?>
                    </td>
                    <td>
                        <?php echo number_format($data->harga_buku,0,',','.')?>
                    </td>
										<td>
											<?php echo number_format($data->banyak_barang,0,',','.')?>
										</td>
										<td>
											Rp. <?php echo number_format($data->total_harga,0,',','.')?>
										</td>
									</tr>
                  <?php endforeach; ?>
                  <?php 
                  	foreach ($totalharga as $total):
                  ?>
                  <tr>
                      <th colspan='5'>TOTAL HARGA</th><th colspan=2>Rp. <?php echo number_format($total->totalharga,0,',','.')?></th>
                  </tr>
                    <?php endforeach;?>
								</tbody>
							</table>          
						</div>
					</div>
				</div>
				<?php if($cekbukti == null):?>
				<form action="<?php echo base_url().'customer/riwayat/simpan_bukti'?>" method="post" enctype="multipart/form-data" >
						<div class="custom-file col-6">
							<label class="custom-file-label" for="customFile" style="font-size: 12px;">Unggah Bukti Transfer</label>
							
							<input type="file" class="custom-file-input" name="filefoto">
							<input type="name" name="xkode" value="<?php echo $kode?>" hidden>
						</div>
						<div class="custom-file col-6" style="height: auto;">
							<input class="btn btn-success" type="submit" name="btn" value="SIMPAN" />
						</div>
					</form>
					<div class="products">
					<?php else:?>
					<button type="button" class="btn btn-primary btn-block">Bukti Transfer Sudah Terkirim</button>
					<?php endif?>

			</div>
			</div>

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>

		
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>