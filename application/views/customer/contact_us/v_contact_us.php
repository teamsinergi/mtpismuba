<!DOCTYPE html>
<html lang="en">
<head>
<title>Penjualan Buku</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Little Closet template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/bootstrap-4.1.2/bootstrap.min.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?> " >
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/main_styles.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets2/styles/responsive.css'); ?> ">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style/style.css'); ?> ">


</head>
<body>
<?php if ($this->session->userdata('email_sekolah')==null){
			 $this->load->view("customer/partials/navbarlogin.php");
	}else{
		$this->load->view("customer/partials/navbar.php");
	}
	?>
<!-- Menu -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<div class="super_container">

	<!-- Header -->



	<div class="super_container_inner">
		<
<?php if($this->session->userdata('nama_sekolah')==null){ ?>
<div class="container contact-form">
            <div class="contact-image">
                <img src="https://image.ibb.co/kUagtU/message_contact.png" alt="rocket_contact"/>
            </div>
            <form action="<?php echo base_url().'customer/contact_us/simpan_pesan'?>" method="post" enctype="multipart/form-data" >
                <h3>Kirim Pesan Anda Kepada Kami</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                             <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-user"></i> Nama</h4>
                            <input type="text" name="xnama" class="form-control" placeholder="Masukkan nama" value="" />
                        </div>
                        <div class="form-group">
                            <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-envelope"></i> E- mail</h4>
                            <input type="email" name="xemail" class="form-control" placeholder="Masukkan E - mail" value="" />
                        </div>
                        <div class="form-group">
                           <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-phone-square"></i> No. Telepon</h4>
                            <input type="number" name="xno" class="form-control" placeholder="Masukkan no. telepon" value="" />
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">

                            <h4 style="color: #fff; font-size: 15px;"><i class="fa fa-comments"></i> Komentar Anda</h4>
                            <textarea name="xpesan" class="form-control" placeholder="Tulis Pesan Anda" style="width: 100%; height: 150px;"></textarea>
                        </div>

                        <div class="form-group">
                                <button class="btn btn-primary"type="submit">KIRIM <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </form>
</div>




 <?php }else{ ?>
<div class="container contact-form">
            <div class="contact-image">
                <img src="<?php echo base_url('assets2/images/paper-plane.png');?>"/>
            </div>
            <form action="<?php echo base_url().'customer/contact_us/simpan_pesan'?>" method="post" enctype="multipart/form-data" >
                <h3>Kirim Pesan Anda Kepada Kami</h3>
               <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-user"></i> Nama</h4>
                            <input type="text" name="xnama" class="form-control" placeholder="Masukkan nama anda" value="" />
                        </div>

                        <div class="form-group">
                            <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-envelope"></i> E- mail</h4>
                            <input type="email" name="xemail" class="form-control" value="<?php echo $this->session->userdata('email_sekolah')?>" readonly/>
                        </div>

                        <div class="form-group">
                            <h4 style="color: #fff; font-size: 15px;"> <i class="fa fa-phone-square"></i> No. Telepon</h4>
                            <input type="number" name="xno" class="form-control" placeholder="Masukkan no. telepon anda" value="" />
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4 style="color: #fff; font-size: 15px;"><i class="fa fa-comments"></i> Komentar Anda</h4>
                            <textarea name="xpesan" class="form-control" placeholder="Masukkan komentar anda" style="width: 100%; height: 150px;"></textarea>
                        </div>


                        <div class="form-group">
                                <button class="btn btn-primary" type="submit">KIRIM <i class="fa fa-paper-plane"></i></button>
                        </div>
                    </div>
                </div>
            </form>
</div>

<?php } ?>
		<!-- Home -->

		<!-- Footer -->

	<?php $this->load->view("customer/partials/footer.php") ?>
	</div>
		
</div>

<script src="assets2/js/jquery-3.2.1.min.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/popper.js"></script>
<script src="assets2/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="assets2/plugins/greensock/TweenMax.min.js"></script>
<script src="assets2/plugins/greensock/TimelineMax.min.js"></script>
<script src="assets2/plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="assets2/plugins/greensock/animation.gsap.min.js"></script>
<script src="assets2/plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="assets2/plugins/easing/easing.js"></script>
<script src="assets2/plugins/parallax-js-master/parallax.min.js"></script>
<script src="assets2/js/cart.js"></script>

</body>
</html>