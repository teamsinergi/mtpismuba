<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class M_transaksi extends CI_Model{

    private $_table = "tbl_transaksi";

    function get_transaksi(){
        return $this->db->get($this->_table)->result();
    }

    function get_all_transaksi(){
        $query = $this->db->query("SELECT tbl_transaksi.id_transaksi, tbl_sekolah.nama_sekolah , tbl_buku.nama_buku, tbl_transaksi.id_sekolah, tbl_transaksi.tanggal, tbl_transaksi.banyak_barang, tbl_transaksi.total_harga 
        FROM tbl_transaksi, tbl_buku, tbl_sekolah
        WHERE tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah");
        return $query->result();
    }

    function get_sekolah(){
        $query = $this->db->query("SELECT * FROM tbl_sekolah");
        return $query->result();
    }

    function get_buku(){
        $query = $this->db->query("SELECT * FROM tbl_buku ORDER BY kelas_buku ASC");
        return $query->result();
    }

    function simpan_transaksi($idsekolah, $idbarang, $banyakbarang, $harga){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_transaksi (id_sekolah, id_barang, banyak_barang, total_harga, tanggal) VALUES('$idsekolah', '$idbarang', '$banyakbarang','$harga'*'$banyakbarang','$tanggal')");
        return $query;
    }
    
    function tampung_transaksi($idsekolah, $idbarang, $banyakbarang, $harga){
        //$tanggal = date("Y-m-d H:i:s");
        $bulan = date("m");
        $menit = date("i");
        $kodetransaksi = $idsekolah.$bulan.$menit;
        $query = $this->db->query("INSERT INTO tbl_tampung_transaksi (id_sekolah, id_barang, banyak_barang, total_harga, kode_transaksi) VALUES('$idsekolah', '$idbarang', '$banyakbarang','$harga'*'$banyakbarang', '$kodetransaksi')");
        return $query;
    }
    

    function delete_sekolah($id){
        $query = $this->db->query("DELETE FROM tbl_transaksi WHERE id_transaksi='$id'");
        return $query;
    }

    function transaksi_kecil(){
        $query = $this->db->query("SELECT tbl_transaksi.id_sekolah, tbl_sekolah.nama_sekolah, tbl_transaksi.kode_transaksi, sum(tbl_transaksi.banyak_barang) as banyak, tbl_transaksi.tanggal, tbl_status_barang.nama as status_barang
            FROM tbl_sekolah, tbl_transaksi, tbl_status_barang 
            WHERE tbl_sekolah.id_sekolah = tbl_transaksi.id_sekolah AND tbl_status_barang.id_status = tbl_transaksi.status_transaksi GROUP BY tbl_transaksi.kode_transaksi ORDER BY tbl_transaksi.tanggal DESC");
        return $query->result();
    }

    function detail_transaksi($where, $kode){
        $query = $this->db->query("SELECT tbl_buku.kelas_buku, tbl_buku.harga_buku, tbl_transaksi.id_transaksi, tbl_transaksi.id_barang, tbl_transaksi.id_sekolah, 
        tbl_sekolah.nama_sekolah, tbl_buku.nama_buku, tbl_buku.harga_buku, tbl_transaksi.banyak_barang, tbl_transaksi.total_harga, tbl_transaksi.kode_transaksi
        FROM tbl_transaksi, tbl_sekolah, tbl_buku
        WHERE tbl_transaksi.id_sekolah=tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_sekolah='$where' AND tbl_transaksi.kode_transaksi ='$kode'");
        return $query->result();
    }


    function detail_harga($where, $kode){
        $query = $this->db->query("SELECT sum(tbl_transaksi.total_harga) as totalharga 
        FROM tbl_transaksi
        WHERE id_sekolah='$where' AND kode_transaksi='$kode'");
        return $query->result();
    }
    function detail_harga_rev($where, $kode){
        $query = $this->db->query("SELECT sum(tbl_transaksi.total_harga) as totalharga 
        FROM tbl_transaksi
        WHERE id_sekolah='$where' AND kode_transaksi='$kode'");
        return $query->row()->totalharga;
    }

    function nama_sekolah($where){
        $query =$this->db->query("SELECT nama_sekolah as sekolah FROM tbl_sekolah WHERE id_sekolah='$where'");
        return $query->row()->sekolah;
    }

    function banyak_transaksi(){
        $query = $this->db->query("SELECT count(*) as totalnya FROM tbl_transaksi GROUP BY kode_transaksi");
        return $query->result();
    }

    function banyak_transaksi_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT count(id_sekolah) as totaslnya FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY id_sekolah,tanggal");
        return $query->result_array();
    }

    function banyak_transaksi_excel($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT count(id_sekolah) as totaslnya FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY id_sekolah,tanggal");
        return $query->result_array();
    }

    function ambil_harga($where){
        $query = $this->db->query("SELECT harga_buku AS harga FROM tbl_buku WHERE id_buku='$where'");
        return $query->row()->harga;
    }

    function total_harganya(){
        $query = $this->db->query("SELECT sum(total_harga) as totalharganya
        FROM tbl_transaksi");
        return $query->result();
    }

    function update_stok($idbarang, $banyakbarang){
        $query = $this->db->query("UPDATE tbl_buku set stok = stok-'$banyakbarang' WHERE id_buku='$idbarang'");
        return $query;
    }

    function simpan_transaksi_pesanan($idsekolah, $idbarang, $banyakbarang, $totalharga, $idtransaksi, $tujuan){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_transaksi(id_sekolah, id_barang, banyak_barang, total_harga, tanggal, kode_transaksi, status_transaksi, tujuan_alamat)
        VALUES('$idsekolah', '$idbarang', '$banyakbarang','$totalharga','$tanggal','$idtransaksi', '3 ','$tujuan')");
        return $query;
    }
    
    function update_laporan_stok($idbarang, $banyakbarang){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_stok_buku(id_buku,stok_sisa, tanggal_update)
        VALUES('$idbarang','$banyakbarang','$tanggal')");
        return $query;
    }

    function edit_transaksi($idtransaksi){
        $query = $this->db->query("SELECT tbl_transaksi.banyak_barang, tbl_buku.nama_buku, tbl_transaksi.banyak_barang, tbl_transaksi.id_transaksi
        FROM tbl_transaksi, tbl_buku WHERE tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_transaksi.id_transaksi = '$idtransaksi'");
        return $query->result();
    }

    function updatetransaksi($id, $banyak){
        $query = $this->db->query("UPDATE tbl_transaksi SET banyak_barang=$banyak WHERE id_transaksi = '$id'");
        return $query;
    }

    function hapustranasksi($id_transaksi, $id_sekolah){
        $query = $this->db->query("DELETE FROM tbl_transaksi WHERE id_transaksi = '$id_transaksi'");
		return $query;
    }

    function lihat_tampung(){
        $query = $this->db->query("SELECT *FROM tbl_tampung_transaksi, tbl_sekolah, tbl_buku WHERE tbl_tampung_transaksi.id_barang = tbl_buku.id_buku AND tbl_tampung_transaksi.id_sekolah = tbl_sekolah.id_sekolah");
        return $query->result();
    }

    function hapustampung(){
        $query = $this->db->query("DELETE FROM tbl_tampung_transaksi");
        return $query;
    }

    function transaksi($kode){
        $query = $this->db->query("SELECT *FROM tbl_transaksi WHERE kode_transaksi='$kode'");
        return $query->result();
    }

    function status_barang(){
        $query = $this->db->query("SELECT *FROM tbl_status_barang");
        return $query->result();
    }

    function update_status($status, $kode){
        $query = $this->db->query("UPDATE tbl_transaksi SET status_transaksi='$status' WHERE kode_transaksi='$kode'");
        return $query;
    }

    function bukti_transfer(){
        $query = $this->db->query("SELECT *FROM tbl_bukti, tbl_transaksi, tbl_sekolah
        WHERE tbl_bukti.kode = tbl_transaksi.kode_transaksi AND tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah GROUP BY tbl_transaksi.kode_transaksi ORDER BY tbl_bukti.tanggal DESC");
        return $query->result();
    }

    function cek_bukti_transfer($kode_transaksi){
        $query = $this->db->query("SELECT *FROM tbl_bukti WHERE kode='$kode_transaksi'");
        return $query->result();
    }

    public function kode_transaksi(){
        $query = $this->db->query("SELECT kode_transaksi FROM tbl_tampung_transaksi");
        return $query->row()->kode_transaksi;
    }
    public function alamat_tujuan(){
        $query = $this->db->query("SELECT tbl_sekolah.alamat_sekolah as alamat FROM tbl_sekolah, tbl_tampung_transaksi
        WHERE tbl_sekolah.id_sekolah = tbl_tampung_transaksi.id_sekolah");
        return $query->row()->alamat;
    }
    public function alamat_pengiriman($kode){
        $query = $this->db->query("SELECT tujuan_alamat FROM tbl_transaksi WHERE kode_transaksi ='$kode'");
        return $query->row()->tujuan_alamat;
    }

    public function tanggal_transaksi($kode){
        $query = $this->db->query("SELECT tanggal FROM tbl_transaksi WHERE kode_transaksi ='$kode'");
        return $query->row()->tanggal;
    }

    public function status_diterima($kode){
        $query = $this->db->query("UPDATE tbl_transaksi SET status_transaksi='2' WHERE kode_transaksi='$kode'");
        return $query;
    }

    public function batalkan($kode){
        $query = $this->db->query("DELETE FROM tbl_pesanan WHERE id_transaksi='$kode'");
        return $query;
    }

    public function total_harga(){
        $query = $this->db->query("SELECT sum(total_harga) as total_harga FROM tbl_tampung_transaksi");
        return $query->row()->total_harga;
    }


}