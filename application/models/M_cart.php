<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_cart extends CI_Model{

    public function tampung_buku($idbuku){
        $email_sekolah = $this->session->userdata('email_sekolah');
        $query = $this->db->query("SELECT *FROM tbl_cart WHERE id_buku='$idbuku' AND email_customer='$email_sekolah'");
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            $email_sekolah = $this->session->userdata('email_sekolah');
            $idcustomer = $this->session->userdata('id_sekolah');
            $query = $this->db->query("INSERT INTO tbl_cart(id_buku, id_pembeli, email_customer) VALUES('$idbuku', '$idcustomer', '$email_sekolah')");
            return $query;
        }
        
        
    }

    public function tampil_tampung_buku(){
        $email_sekolah = $this->session->userdata('email_sekolah');
        $query = $this->db->query("SELECT tbl_cart.id_buku, tbl_cart.jumlah_buku, tbl_cart.id_pembeli, tbl_cart.email_customer, tbl_cart.total_harga from tbl_cart WHERE tbl_cart.email_customer='$email_sekolah'");
        return $query->result();
    }

    public function update_cart($idbarang, $banyakbarang, $harga){
        $email = $this->session->userdata('email_sekolah');
        $query = $this->db->query("UPDATE tbl_cart set  jumlah_buku='$banyakbarang', total_harga='$banyakbarang'*'$harga' WHERE id_buku = '$idbarang' AND email_customer='$email'");
        return $query;
    }

    public function hapusbuku($idbuku){
        $email_sekolah = $this->session->userdata('email_sekolah');
        $query = $this->db->query("DELETE FROM tbl_cart WHERE id_buku = '$idbuku' AND email_customer ='$email_sekolah'");
        return $query;
    }

    public function cart_total(){
        $email_sekolah = $this->session->userdata('email_sekolah');
        $query = $this->db->query("SELECT count(tbl_cart.id_buku) as banyak_buku, sum(tbl_cart.jumlah_buku) as jumlah_buku, sum(tbl_cart.total_harga) as total_harga
        FROM tbl_cart WHERE tbl_cart.email_customer='$email_sekolah'");
        return $query->result();
    }

    // public function simpan_konfirmasi_transaksi($idbarang, $banyakbarang, $total, $idpembeli){
    //     $email = $this->session->userdata('email_sekolah');
    //     $tanggal = date("Y-m-d H:i:s");
    //     $query = $this->db->query("INSERT INTO tbl_cart_konfirmasi(id_buku,jumlah_buku,total_harga,id_pembeli, email_pembeli, tanggal) VALUES ('$idbarang','$banyakbarang','$total','$idpembeli','$email', '$tanggal')");
    //     return $query;
    // }

    public function kirim_pesanan($idbarang, $banyakbarang, $total, $idpembeli, $tujuan){
        $email = $this->session->userdata('email_sekolah');
        $tanggal = date("Y-m-d H:i:s");
        $bulan = date("m");
        $menit = date("i");
        $idtransaksi = $idpembeli.$bulan.$menit;
        $query = $this->db->query("INSERT INTO tbl_pesanan(id_buku,jumlah_buku,total_harga, id_pembeli,email_pembeli, tanggal, id_transaksi, tujuan_alamat) VALUES ('$idbarang','$banyakbarang','$total','$idpembeli','$email', '$tanggal', '$idtransaksi','$tujuan')");
        return $query;
    }

    public function delete_pesanan_customer(){
        $email = $this->session->userdata('email_sekolah');
        $query = $this->db->query("DELETE FROM tbl_cart WHERE email_customer='$email'");
        return $query;
    }

    // ini buat rincian titip bentar

    function detail_transaksi($kode){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT tbl_buku.kelas_buku, tbl_buku.harga_buku, tbl_transaksi.id_transaksi, tbl_transaksi.id_barang, tbl_transaksi.id_sekolah, 
        tbl_sekolah.nama_sekolah, tbl_buku.nama_buku, tbl_buku.harga_buku, tbl_transaksi.banyak_barang, tbl_transaksi.total_harga
        FROM tbl_transaksi, tbl_sekolah, tbl_buku
        WHERE tbl_transaksi.id_sekolah=tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_sekolah='$where' AND tbl_transaksi.kode_transaksi ='$kode'");
        return $query->result();
    }

    function detail_transaksi_pesanan($kode){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT tbl_buku.kelas_buku, tbl_buku.harga_buku, tbl_pesanan.id_transaksi, tbl_pesanan.id_buku as id_barang, tbl_pesanan.id_pembeli as id_sekolah, 
        tbl_sekolah.nama_sekolah, tbl_buku.nama_buku, tbl_buku.harga_buku, tbl_pesanan.jumlah_buku as banyak_barang , tbl_pesanan.total_harga
        FROM tbl_pesanan, tbl_sekolah, tbl_buku
        WHERE tbl_pesanan.id_pembeli=tbl_sekolah.id_sekolah AND tbl_pesanan.id_buku = tbl_buku.id_buku AND tbl_sekolah.id_sekolah='$where' AND tbl_pesanan.id_transaksi ='$kode'");
        return $query->result();
    }

    function detail_harga($kode){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_transaksi.total_harga) as totalharga 
        FROM tbl_transaksi
        WHERE id_sekolah='$where' AND kode_transaksi='$kode'");
        return $query->result();
    }

    function detail_harga_pesanan($kode){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_pesanan.total_harga) as totalharga 
        FROM tbl_pesanan
        WHERE id_pembeli='$where' AND id_transaksi='$kode'");
        return $query->result();
    }

    function status_barang($kode_transaksi){
        $query = $this->db->query("SELECT tbl_status_barang.nama as status_barang FROM tbl_transaksi, tbl_status_barang WHERE tbl_transaksi.status_transaksi = tbl_status_barang.id_status AND tbl_transaksi.kode_transaksi ='$kode_transaksi' LIMIT 1");
        return $query->row()->status_barang;
    }
    function kode_status($kode_transaksi){
        $query = $this->db->query("SELECT tbl_status_barang.id_status as id_status FROM tbl_transaksi, tbl_status_barang WHERE tbl_transaksi.status_transaksi = tbl_status_barang.id_status AND tbl_transaksi.kode_transaksi ='$kode_transaksi' LIMIT 1");
        return $query->row()->id_status;
    }

    function simpan_bukti($kode, $photo){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_bukti(kode,foto_bukti,tanggal) VALUES ('$kode','$photo','$tanggal')");
        return $query;
    }

    public function alamat_customer(){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT alamat_sekolah FROM tbl_sekolah WHERE id_sekolah='$where'");
        return $query->row()->alamat_sekolah;

    }

}