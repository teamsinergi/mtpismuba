<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_pesanan extends CI_Model{

    public function pesananmasuk(){

        $query = $this->db->query("SELECT *FROM tbl_pesanan GROUP BY id_transaksi");
        return $query->result();
    }
    public function detail_pesanan($idpembeli, $id_transaksi){
        $query = $this->db->query("SELECT tbl_pesanan.jumlah_buku, tbl_pesanan.id_transaksi, tbl_pesanan.id_buku, tbl_pesanan.total_harga, tbl_buku.nama_buku, tbl_buku.kelas_buku, tbl_buku.harga_buku
        FROM tbl_pesanan, tbl_buku WHERE tbl_buku.id_buku = tbl_pesanan.id_buku AND tbl_pesanan.id_pembeli ='$idpembeli' AND tbl_pesanan.id_transaksi='$id_transaksi'");
        return $query->result();
    }

    public function detail_pembeli($idpembeli, $id_transaksi){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah, tbl_sekolah.no_hp, tbl_sekolah.id_sekolah, tbl_sekolah.alamat_sekolah, tbl_pesanan.id_transaksi
        FROM tbl_pesanan, tbl_sekolah WHERE tbl_sekolah.id_sekolah=tbl_pesanan.id_pembeli AND tbl_sekolah.id_sekolah='$idpembeli' AND tbl_pesanan.id_transaksi='$id_transaksi' GROUP BY tbl_pesanan.id_transaksi");
        return $query->result();
    }

    public function total_harga($idpembeli, $id_transaksi){
        $query = $this->db->query("SELECT sum(total_harga) as total_harga
        FROM tbl_pesanan WHERE id_pembeli='$idpembeli' AND id_transaksi='$id_transaksi'");
        return $query->result();
    }

    public function hapus_pesanan($idsekolah, $idtransaksi){
        $query = $this->db->query("DELETE FROM tbl_pesanan WHERE id_pembeli='$idsekolah' AND id_transaksi='$idtransaksi'");
        return $query;
    }

    public function alamat_tujuan($id_transaksi){
        $query = $this->db->query("SELECT tujuan_alamat FROM tbl_pesanan WHERE id_transaksi='$id_transaksi'");
        return $query->row()->tujuan_alamat;
    }


}