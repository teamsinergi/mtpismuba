<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_kabupaten extends CI_Model{

    private $_table = "tbl_kabupaten";

    public function getAll()
    {
        $query = $this->db->query("SELECT regencies.id as id_kabupaten, regencies.name as nama_kabupaten, provinces.id, provinces.name as nama_provinsi
        FROM regencies,provinces WHERE regencies.province_id = provinces.id");
        return $query->result();
    }

    public function simpan_kabupaten($nama, $provinsi)
    {
        $tanggal = date("Y-m-d H:i:s");
        $querysimpankabupaten = $this->db->query("INSERT INTO tbl_kabupaten(nama_kabupaten, provinsi) VALUES('$nama','$provinsi')");
        return $querysimpankabupaten;
    }

    public function edit_kabupaten($where, $table){
        return $this->db->get_where($table, $where)->result();
    }

    public function update_kabupaten($id, $nama, $provinsi){
        $query = $this->db->query("UPDATE tbl_kabupaten set nama_kabupaten='$nama', provinsi='$provinsi' WHERE id_kabupaten='$id'");
        return $query;
    }

    public function hapus($where, $table){
        $this->db->where($where);
		$this->db->delete($table);
    }

    public function delete_kabupaten($id){
        $query = $this->db->query("DELETE FROM tbl_kabupaten WHERE id_kabupaten = '$id'");
        return $query;
    }
    function nama_kabupatennya($id){
        $query = $this->db->query("SELECT regencies.name as nama_kabupaten FROM tbl_sekolah, regencies WHERE tbl_sekolah.id_sekolah = '$id' AND tbl_sekolah.id_kabupaten = regencies.id");
        return $query->row()->nama_kabupaten;
    }
    function id_kabupatennya($id){
        $query = $this->db->query("SELECT regencies.id as id_kabupaten FROM tbl_sekolah, regencies WHERE tbl_sekolah.id_sekolah = '$id' AND tbl_sekolah.id_kabupaten = regencies.id");
        return $query->row()->id_kabupaten;
    }

    public function get_all_provinsi(){
        $q = $this->db->query("SELECT *FROM provinces");
        return $q->result();
    }

    

}