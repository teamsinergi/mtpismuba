<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class M_sekolah extends CI_Model{

    private $_table = "tbl_sekolah";
    
    function get_all_sekolah(){
        return $this->db->get($this->_table)->result();
    }

    function get_all_sekolah1(){
        $query = $this->db->query("SELECT tbl_sekolah.id_sekolah, tbl_sekolah.nama_sekolah, tbl_sekolah.alamat_sekolah, tbl_sekolah.id_kabupaten, regencies.name, tbl_sekolah.email_sekolah, tbl_sekolah.no_hp, tbl_level_customer.nama_level 
        FROM regencies, tbl_sekolah, tbl_level_customer WHERE tbl_sekolah.id_kabupaten = regencies.id AND tbl_sekolah.id_level = tbl_level_customer.id_level");
        return $query->result();
    }

    function get_all_kabupaten(){
        $query = $this->db->query("SELECT * FROM tbl_kabupaten");
        return $query->result();
    }

    public function simpan_sekolah($namasekolah, $alamatsekolah, $idkabupaten, $email, $nohp, $pass, $level){
        // $level = 1;
        $active = 1;
        $query = $this->db->query("INSERT INTO tbl_sekolah(nama_sekolah, alamat_sekolah, id_kabupaten, email_sekolah, no_hp, pembuat, password_sekolah, id_level, active) VALUES ('$namasekolah', '$alamatsekolah', '$idkabupaten', '$email','$nohp','$level','$pass','$level', '$active')");
        return $query;
    }

    public function delete_sekolah($idsekolah){
        $query = $this->db->query("DELETE FROM tbl_sekolah WHERE id_sekolah='$idsekolah'");
        return $query;
    }

    public function edit_sekolah($where, $table){
        return $this->db->get_where($table, $where);
    }

    public function update_sekolah($id, $nama, $alamat, $idkab, $email, $nohp, $pass, $level){
        $query = $this->db->query("UPDATE tbl_sekolah set nama_sekolah='$nama', alamat_sekolah='$alamat', id_kabupaten='$idkab', email_sekolah='$email', no_hp='$nohp', password_sekolah='$pass', id_level='$level'
        WHERE id_sekolah='$id'");
        return $query;
    }

    public function delete_transaksi_sekolah($idsekolah){
        $query = $this->db->query("DELETE FROM tbl_transaksi WHERE id_sekolah='$idsekolah'");
        return $query;
    }


    // dibawah ini buat login user

    public function email_check($email){

        $this->db->select('*');
        $this->db->from('tbl_sekolah');
        $this->db->where('user_email',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    function register_customer(){
        $this->db->insert('tbl_customer', $user);
    }

    public function tambah_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer){
        $query = $this->db->query("INSERT INTO tbl_sekolah(nama_sekolah,alamat_sekolah,email_sekolah,no_hp,id_kabupaten)
        VALUES('$nama_sekolah','$alamat_customer','$email_sekolah','$no_hp','$id_kabupaten')");
        return $query;
    }

    public function ambil_id($email_sekolah){
        $this->db->select('id_sekolah');
        $this->db->from('tbl_sekolah');
        $this->db->where('email_sekolah',$email_sekolah);
        return $this->db->get()->row()->id_sekolah;
    }

    // ini buat costomer
    public function view_profile(){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT provinces.name as prov, regencies.name as kab, tbl_sekolah.no_hp, tbl_sekolah.email_sekolah, tbl_sekolah.nama_sekolah, tbl_sekolah.alamat_sekolah, tbl_sekolah.password_sekolah FROM tbl_sekolah, regencies, provinces
        WHERE id_sekolah='$where' AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.province_id = provinces.id");
        return $query->result();
    }

    public function nama_pelanggan(){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT nama_sekolah FROM tbl_sekolah WHERE id_sekolah='$where'");
        return $query->row()->nama_sekolah;
    }

    // ini buat update profile
    public function update_user($nama, $no, $alamat, $email, $password, $kabupaten){
        $where = $this->session->userdata('id_sekolah');
        $query = $this->db->query("UPDATE tbl_sekolah set nama_sekolah='$nama', no_hp='$no', alamat_sekolah='$alamat', email_sekolah='$email', password_sekolah='$password', id_kabupaten ='$kabupaten'
        WHERE id_sekolah='$where'");
        return $query;
    }

    public function level_customer(){
        $query = $this->db->query("SELECT *FROM tbl_level_customer");
        return $query->result();
    }
    public function id_levelnya($id){
        $query = $this->db->query("SELECT tbl_sekolah.id_level as id_level FROM tbl_sekolah, tbl_level_customer
        WHERE tbl_sekolah.id_level = tbl_level_customer.id_level AND tbl_sekolah.id_sekolah = '$id'");
        return $query->row()->id_level;
    }
    public function nama_levelnya($id){
        $query = $this->db->query("SELECT tbl_level_customer.nama_level as nama_level FROM tbl_sekolah, tbl_level_customer
        WHERE tbl_sekolah.id_level = tbl_level_customer.id_level AND tbl_sekolah.id_sekolah = '$id'");
        return $query->row()->nama_level;
    }

    public function get_all_provinsi(){
        $q = $this->db->query("SELECT *FROM provinces");
        return $q->result();
    }
    public function kabupaten($email){
        $query = $this->db->query("SELECT tbl_sekolah.id_kabupaten, regencies.name FROM regencies, tbl_sekolah WHERE tbl_sekolah.id_kabupaten = regencies.id AND tbl_sekolah.email_sekolah='$email'");
        return $query->result();
    }
}