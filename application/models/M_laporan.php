<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_laporan extends CI_Model{

    public function tampil_semua(){
        $query = $this->db->query("SELECT tbl_transaksi.id_sekolah, tbl_sekolah.nama_sekolah, sum(tbl_transaksi.banyak_barang) as banyak, sum(tbl_transaksi.total_harga) as totalharga, regencies.name
        FROM regencies, tbl_transaksi, tbl_sekolah, tbl_buku WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_kabupaten = regencies.id GROUP BY id_sekolah ORDER BY regencies.name ASC");
        return $query->result();
    }

    public function tampil_semua_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT tbl_transaksi.id_sekolah, tbl_sekolah.nama_sekolah, sum(tbl_transaksi.banyak_barang) as banyak, sum(tbl_transaksi.total_harga) as totalharga, regencies.name
        FROM regencies, tbl_transaksi, tbl_sekolah, tbl_buku WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_kabupaten = regencies.id AND year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY id_sekolah ORDER BY regencies.name ASC");
        return $query->result();
    }
    public function tampil_semua_excel($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT tbl_transaksi.id_sekolah, tbl_sekolah.nama_sekolah, sum(tbl_transaksi.banyak_barang) as banyak, sum(tbl_transaksi.total_harga) as totalharga, regencies.name
        FROM regencies, tbl_transaksi, tbl_sekolah, tbl_buku WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_kabupaten = regencies.id AND year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY id_sekolah ORDER BY regencies.name ASC");
        return $query->result_array();
    }

    public function total_buku_terjual_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT sum(banyak_barang) AS totalbukuterjual FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai'");
        return $query->result();
    }
    public function total_buku_terjual_excel($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT sum(banyak_barang) AS totalbukuterjual FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai'");
        return $query->result_array();
    }
    public function total_pendapatan_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT sum(total_harga) as totalharga
        FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai'");
        return $query->result();
    }

    public function total_pendapatan_excel($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT sum(total_harga) as totalharga
        FROM tbl_transaksi WHERE year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai'");
        return $query->result_array();
    }

    public function getkabupaten_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT regencies.name as nama_kabupaten, tbl_sekolah.id_kabupaten, SUM(total_harga) AS keuntungan, SUM(banyak_barang) AS totalterjual
        FROM regencies, tbl_sekolah, tbl_transaksi
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY regencies.id");
        return $query->result();
    }

    public function sekolah_kabupaten_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah, tbl_sekolah.id_kabupaten, sum(tbl_transaksi.banyak_barang) as jumlahbarang, sum(tbl_transaksi.total_harga) as totalharganya
        FROM regencies, tbl_sekolah, tbl_transaksi, tbl_buku
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND tbl_buku.id_buku = tbl_transaksi.id_barang AND year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY tbl_sekolah.id_sekolah");
        return $query->result();
    }

    public function laporan_buku_x($tahundari, $tahunsampai){
        $query = $this->db->query("SELECT tbl_buku.nama_buku, tbl_buku.kelas_buku, SUM(tbl_transaksi.banyak_barang) AS banyak_barang, tbl_buku.stok
        FROM tbl_buku, tbl_transaksi
        WHERE tbl_buku.id_buku = tbl_transaksi.id_barang AND year(tanggal)>='$tahundari' and year(tanggal)<='$tahunsampai' GROUP BY tbl_buku.id_buku");
        return $query->result();
    }

    public function total_buku_terjual(){
        $query = $this->db->query("SELECT sum(banyak_barang) AS totalbukuterjual FROM tbl_transaksi");
        return $query->result();
    }


    public function total_pendapatan(){
        $query = $this->db->query("SELECT sum(total_harga) as totalharga
        FROM tbl_transaksi");
        return $query->result();
    }

    public function getkabupaten(){
        $query = $this->db->query("SELECT regencies.name as nama_kabupaten, tbl_sekolah.id_kabupaten, SUM(total_harga) AS keuntungan, SUM(banyak_barang) AS totalterjual
        FROM regencies, tbl_sekolah, tbl_transaksi
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id GROUP BY regencies.id");
        return $query->result();
    }

    public function sekolah_kabupaten(){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah, tbl_sekolah.id_kabupaten, sum(tbl_transaksi.banyak_barang) as jumlahbarang, sum(tbl_transaksi.total_harga) as totalharganya
        FROM regencies, tbl_sekolah, tbl_transaksi, tbl_buku
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND tbl_buku.id_buku = tbl_transaksi.id_barang GROUP BY tbl_sekolah.id_sekolah");
        return $query->result();
    }

    public function pilih_kabupaten(){
        $query = $this->db->query("SELECT regencies.name as nama_kabupaten, regencies.id as id_kabupaten
        FROM tbl_transaksi, tbl_sekolah, regencies
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id GROUP BY regencies.id");
        return $query->result();
    }

    public function cetak_buku(){
        return $this->db->get('tbl_buku')->result();
    }

    public function ambil_total(){
        for($i=0; $i<100; $i++){
        $query = $this->db->query("SELECT count(tbl_transaksi.id_barang) as jumlahbarang, tbl_kabupaten.id_kabupaten
        from tbl_transaksi, tbl_buku, tbl_sekolah, tbl_kabupaten 
        where tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = tbl_kabupaten.id_kabupaten AND tbl_kabupaten.id_kabupaten='$i'");
        return $query->result();
        }
    }

    public function laporan_buku(){
        $query = $this->db->query("SELECT tbl_buku.nama_buku, tbl_buku.kelas_buku, SUM(tbl_transaksi.banyak_barang) AS banyak_barang, tbl_buku.stok
        FROM tbl_buku, tbl_transaksi
        WHERE tbl_buku.id_buku = tbl_transaksi.id_barang GROUP BY tbl_buku.id_buku");
        return $query->result();
    }


    // dibawah ini model buat cetak perkabupaten

    public function nama_sekolah_kabupaten($id_kabupaten, $bulandari, $tahundari, $bulansampai){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah
        FROM regencies, tbl_sekolah
        WHERE regencies.id = tbl_sekolah.id_kabupaten AND regencies.id='$id_kabupaten'");
        return $query->result();
    }

    public function total_buku_terjual_kab($id_kabupaten, $bulandari, $tahundari, $bulansampai){
        $query = $this->db->query("SELECT count(id_barang) AS totalbukuterjual, regencies.name as nama_kabupaten 
        FROM tbl_transaksi,tbl_sekolah, regencies 
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.id = '$id_kabupaten' AND month(tanggal)>='$bulandari' and year(tanggal)>='$tahundari' AND month(tanggal)<='$bulansampai'");
        return $query->result();
    }

    public function total_pendapatan_kab($id_kabupaten, $bulandari, $tahundari, $bulansampai){
        $query = $this->db->query("SELECT sum(tbl_transaksi.total_harga) as totalharga
        FROM tbl_buku, tbl_transaksi, tbl_sekolah, regencies
        WHERE tbl_buku.id_buku = tbl_transaksi.id_barang AND tbl_sekolah.id_sekolah = tbl_transaksi.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.id = '$id_kabupaten' AND month(tanggal)>='$bulandari' and year(tanggal)>='$tahundari' AND month(tanggal)<='$bulansampai'");
        return $query->result();
    }

    public function tampil_semua_kab($id_kabupaten, $bulandari, $tahundari, $bulansampai){
        $query = $this->db->query("SELECT tbl_transaksi.id_sekolah, tbl_sekolah.nama_sekolah, sum(tbl_transaksi.banyak_barang) as banyak, sum(tbl_transaksi.total_harga) as totalharga, regencies.name as nama_kabupaten 
        FROM regencies, tbl_transaksi, tbl_sekolah, tbl_buku 
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_transaksi.id_barang = tbl_buku.id_buku AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.id = '$id_kabupaten' AND month(tanggal)>='$bulandari' and year(tanggal)>='$tahundari' AND month(tanggal)<='$bulansampai' GROUP BY id_sekolah");
        return $query->result();
    }

    public function banyak_buku(){
        $query = $this->db->query("SELECT tbl_buku.nama_buku, tbl_buku.kelas_buku, tbl_transaksi.banyak_barang, tbl_transaksi.id_sekolah
        FROM tbl_buku, tbl_transaksi WHERE tbl_buku.id_buku = tbl_transaksi.id_barang");
        return $query->result();
    }

    // ini buat buku laporan
    function laporan_bukuku(){
        $query = $this->db->query("SELECT tbl_buku.id_buku, tbl_buku.nama_buku, tbl_buku.kelas_buku, tbl_buku.harga_buku, tbl_stok_buku.stok_awal, tbl_stok_buku.stok_sisa
        FROM tbl_stok_buku, tbl_buku WHERE tbl_buku.id_buku = tbl_stok_buku.id_buku GROUP BY tbl_buku.id_buku ORDER BY kelas_buku ASC");
        return $query->result();
    }

    public function stok_2019($tahun){
        $query = $this->db->query("SELECT tbl_buku.id_buku, sum(tbl_stok_buku.stok_awal) as stok_awal, sum(tbl_stok_buku.stok_sisa) as stok_sisa
        FROM tbl_buku, tbl_stok_buku WHERE tbl_buku.id_buku = tbl_stok_buku.id_buku AND year(tbl_stok_buku.tanggal_update)='$tahun' GROUP BY tbl_stok_buku.id_buku");
        return $query->result();
    }

    public function rekapan_buku($tahun){
        $query = $this->db->query("SELECT sum(stok_awal) as stok_awal, sum(stok_sisa) as stok_sisa, id_buku
        FROM tbl_stok_buku WHERE year(tbl_stok_buku.tanggal_update)='$tahun'");
        return $query->result();
    }

    public function laporan_sisa($tahun){
        $tahunsisa = $tahun-1;
        $query = $this->db->query("SELECT tbl_buku.id_buku, sum(tbl_stok_buku.stok_awal) as stok_awal, sum(tbl_stok_buku.stok_sisa) as stok_sisa , tbl_sisa_buku.sisa as sisa
        FROM tbl_buku, tbl_stok_buku, tbl_sisa_buku WHERE tbl_buku.id_buku = tbl_stok_buku.id_buku AND tbl_sisa_buku.id_buku = tbl_buku.id_buku AND year(tbl_stok_buku.tanggal_update)='$tahunsisa' AND year(tbl_sisa_buku.tanggal)='$tahunsisa' GROUP BY tbl_stok_buku.id_buku");
        // $query = $this->db->query("SELECT tbl_buku.id_buku, tbl_sisa_buku.sisa as stok_sisa
        // FROM tbl_buku, tbl_sisa_buku WHERE tbl_buku.id_buku = tbl_sisa_buku.id_buku AND year(tbl_sisa_buku.tanggal)='$tahunsisa'");
        return $query->result();
    }

    public function filter_lelve(){
        $query = $this->db->query("SELECT *FROM tbl_buku GROUP BY kelas_buku");
        return $query->result();
    }

    public function tahun_buku($id){
        $tahunsekarang = date("Y");
        $query = $this->db->query("SELECT year(tanggal) as tahunbukusisa, id_buku FROM tbl_sisa_buku WHERE year(tanggal)=$tahunsekarang AND id_buku='$id'");
        return $query->result();
    }

    public function ambil_nilai($i){
        $tahun = date("Y");
        $query = $this->db->query("SELECT sum(banyak_barang) as banyak_barang FROM tbl_transaksi WHERE MONTH(tanggal)=$i AND YEAR(tanggal) = $tahun");
        return $query->row()->banyak_barang;
    }

    public function laporan_perkabupaten($tahundari){
        $query = $this->db->query("SELECT regencies.name , sum(tbl_transaksi.banyak_barang) as banyakterjual, sum(tbl_transaksi.total_harga) as totalharga 
        from tbl_transaksi, tbl_sekolah, regencies 
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND year(tanggal) = '$tahundari'
        GROUP BY regencies.id");
        return $query->result_array();
    }

    public function buku_terjual($tahundari){
        $query = $this->db->query("SELECT tbl_buku.nama_buku, tbl_buku.harga_buku, tbl_buku.kelas_buku, sum(tbl_transaksi.banyak_barang) as banyak_barang, sum(total_harga) as total_harga
        FROM tbl_buku, tbl_transaksi WHERE tbl_buku.id_buku = tbl_transaksi.id_barang AND year(tbl_transaksi.tanggal)='$tahundari' GROUP BY tbl_transaksi.id_barang");
        return $query->result_array();
    }

    // public function getprovinsi(){
    //     $query = $this->db->query("SELECT provinces.id, provinces.name, sum(tbl_transaksi.banyak_barang) as banyak_barang, sum(total_harga) as total_harga, provinces.id as id_kabupaten
    //     FROM tbl_transaksi, provinces, tbl_sekolah, regencies
    //     WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.province_id = provinces.id");
    //     return $query->result();
    // }
    public function getprovinsi(){
        $query = $this->db->query("SELECT provinces.id, provinces.name, provinces.id as id_kabupaten
        FROM tbl_transaksi, provinces, tbl_sekolah, regencies
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.province_id = provinces.id GROUP BY provinces.id");
        return $query->result();
    }

    public function kab_prov(){
        $query = $this->db->query("SELECT tbl_sekolah.nama_sekolah, tbl_sekolah.id_kabupaten, sum(tbl_transaksi.banyak_barang) as jumlahbarang, sum(tbl_transaksi.total_harga) as totalharganya, provinces.id as id
        FROM regencies, tbl_sekolah, tbl_transaksi, tbl_buku, provinces
        WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND tbl_buku.id_buku = tbl_transaksi.id_barang AND regencies.province_id = provinces.id GROUP BY tbl_sekolah.id_sekolah");
        return $query->result();
    }

    public function laporan_perpro($tahun){
        $query = $this->db->query("SELECT provinces.name, sum(tbl_transaksi.banyak_barang) as banyak_barang, sum(tbl_transaksi.total_harga) as total_harga FROM tbl_sekolah, tbl_transaksi, regencies, provinces WHERE tbl_transaksi.id_sekolah = tbl_sekolah.id_sekolah AND tbl_sekolah.id_kabupaten = regencies.id AND regencies.province_id=provinces.id AND year(tbl_transaksi.tanggal)='$tahun' GROUP BY provinces.id");
        return $query->result_array();
    }
}