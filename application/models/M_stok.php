<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class M_stok extends CI_Model{

    function update_sisa($id_buku, $sisa){
        $tanggal = date("Y-m-d H:i:s");
        $tahunsekarang = date("Y");
        $query = $this->db->query("UPDATE tbl_sisa_buku set sisa='$sisa', tanggal='$tanggal' WHERE id_buku='$id_buku' AND year(tanggal)='$tahunsekarang'");
        return $query;
    }

    function tambah_sisa($id_buku, $sisa){
        $tanggal = date("Y-m-d H:i:s");
        $query = $this->db->query("INSERT INTO tbl_sisa_buku(id_buku,sisa,tanggal) VALUES('$id_buku','$sisa','$tanggal')");
        return $query;

    }

    function simpan_sisa($idbukunya, $sisa){
        $query = $this->db->query("INSERT INTO tbl_sisa_buku(id_buku,sisa) VALUES ('$idbukunya', '$sisa')");
        return $query;
    }

    function simpan_tahun_sebelumnya_sisa($idbukunya, $stok){
        $sisa = 0;
        $tanggal = date('Y-m-d', strtotime('-1 years'));
        $query = $this->db->query("INSERT INTO tbl_sisa_buku(id_buku,sisa, tanggal) VALUES ('$idbukunya', '$sisa', '$tanggal' )");
        return $query;
    }
}