<?php
class M_customer extends CI_model{

    public function register_customer($nama_sekolah,$alamat_customer, $email_sekolah, $no_hp, $id_kabupaten, $password_customer, $code, $level){
        $query = $this->db->query("INSERT INTO tbl_sekolah(nama_sekolah, alamat_sekolah, email_sekolah, no_hp, id_kabupaten, pembuat, password_sekolah, code, id_level) 
        VALUES('$nama_sekolah','$alamat_customer','$email_sekolah','$no_hp','$id_kabupaten','$level', '$password_customer','$code','$level')");
        return $query;
    }

    public function login_user_costomer($email,$pass){

        $this->db->select('*');
        $this->db->from('tbl_sekolah');
        $this->db->where('email_sekolah',$email);
        $this->db->where('password_sekolah',$pass);

        if($query=$this->db->get())
        {
            return $query->row_array();
        }
        else{
            return false;
        }
    }
    public function email_check($email){

        $this->db->select('*');
        $this->db->from('tbl_sekolah');
        $this->db->where('email_sekolah',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return false;
        }else{
            return true;
        }
    }

    public function email_checkpass($email){
        $query = $this->db->query("SELECT email_sekolah FROM tbl_sekolah WHERE email_sekolah='$email'");
        return $query->result();
    }
    

    public function update_id($email_sekolah,$id_pelanggan){
        $query = $this->db->query("UPDATE tbl_customer set id_customer='$id_pelanggan' WHERE email_sekolah='$email_sekolah'");
        return $query;
    }

    public function riwayat_customer(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_transaksi.banyak_barang) as banyak_barang,tbl_transaksi.kode_transaksi, tbl_transaksi.tanggal, sum(tbl_transaksi.total_harga) as total_harga, tbl_transaksi.status_transaksi AS status 
        FROM tbl_transaksi WHERE tbl_transaksi.id_sekolah='$idcustomer' GROUP BY tbl_transaksi.kode_transaksi ORDER BY tbl_transaksi.tanggal DESC");
        return $query->result();
    }

    public function riwayat_proses(){
        $idcustomer = $this->session->userdata('id_sekolah');
        $query = $this->db->query("SELECT sum(tbl_pesanan.jumlah_buku) as banyak_barang, tbl_pesanan.tanggal, sum(tbl_pesanan.total_harga) as total_harga, tbl_pesanan.id_transaksi as kode_transaksi 
        FROM tbl_pesanan WHERE tbl_pesanan.id_pembeli='$idcustomer' GROUP BY tbl_pesanan.tanggal ORDER BY tbl_pesanan.tanggal DESC");
        return $query->result();
    }

    public function ambil_id($email){
        $query = $this->db->query("SELECT id_sekolah FROM tbl_sekolah WHERE email_sekolah='$email'");
        return $query->row()->id_sekolah;
    }

    function getUser($id){
        $query = $this->db->get_where('tbl_sekolah',array('id_sekolah'=>$id));
		return $query->row_array();
    }

    function activin($id){
        $status = 1;
        $query = $this->db->query("UPDATE tbl_sekolah set active='$status' WHERE id_sekolah='$id'");
        return $query;
    }

    function ambil_status($email){
        $query = $this->db->query("SELECT active FROM tbl_sekolah WHERE email_sekolah='$email'");
        return $query->row()->active;
    }

    function ambil_email($id){
        $query = $this->db->query("SELECT email_sekolah FROM tbl_sekolah WHERE id_sekolah='$id'");
        return $query->row()->email_sekolah;
    }
    
    function passworbaru($id, $email_sekolah, $password){
        $query = $this->db->query("UPDATE tbl_sekolah set password_sekolah='$password' WHERE id_sekolah='$id'");
        return $query;
    }


}
?>